
tdaq_package()

remove_definitions(-DERS_NO_DEBUG)

find_package(Boost)
include_directories(${Boost_INCLUDE_DIRS})

tdaq_add_library(dqm_core 
  src/*.cpp  
  dqm_core.cpp
  LINK_LIBRARIES ROOT::Core ROOT::RIO ers Boost::thread Boost::date_time)

tdaq_root_generate_dictionary(dqm_core
  dqm_core/AlgorithmConfig.h 
  dqm_core/Result.h 
  dqm_core/Algorithm.h
  OPTIONS -I${CMAKE_SOURCE_DIR}/ers
  LINKDEF dqm_core/LinkDef.h)

tdaq_add_library(dqm_core_io 
  src/io/InputRootFile.cpp 
  LINK_LIBRARIES dqm_core ROOT::Core ers  Boost::thread)

tdaq_add_library(dqm_dummy 
  test/DummyAlgorithmConfig.cpp 
  test/DummyAlgorithm.cpp 
  test/DummySummary.cpp 
  dqm_dummy.cpp
  LINK_LIBRARIES ROOT::Core ROOT::Hist dqm_core ers)

tdaq_root_generate_dictionary(dqm_dummy
  dqm_core/test/DummyAlgorithmConfig.h 
  dqm_core/test/DummyAlgorithm.h 
  OPTIONS -I${CMAKE_SOURCE_DIR}/ers
  LINKDEF dqm_core/test/LinkDef.h)

tdaq_add_library(dqm_dummy_io 
  test/DummyInput.cpp test/DummyOutput.cpp
  LINK_LIBRARIES ROOT::Core  ROOT::Hist dqm_core ers Boost::thread)

tdaq_add_executable(dqm_core_test 
  test/test_core.cpp 
  LINK_LIBRARIES ROOT::Core ROOT::Hist dqm_dummy dqm_core ers Boost::thread Boost::program_options TBB)

tdaq_add_executable(dqm_core_algorithm_test 
  test/test_algorithm.cpp
  LINK_LIBRARIES ROOT::Core ROOT::Hist dqm_core dqm_dummy ers Boost::thread Boost::program_options TBB)

tdaq_add_pcm(dqm_core dqm_dummy)
