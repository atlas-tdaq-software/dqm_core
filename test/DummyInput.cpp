/*! \file DummyInput.cpp contains the dqm_core::test::DummyInput class implementation.
 * \author Serguei Kolos
 * \version 1.0
 */

#include <dqm_core/test/DummyInput.h>
#include <dqm_core/InputListener.h>
#include <dqm_core/InputFactoryT.h>

#include <TH1.h>

namespace {
  dqm_core::InputFactoryT<dqm_core::test::DummyInput> __factory__("DummyInput");
}

dqm_core::test::DummyInput::DummyInput(const std::vector<std::string> & params) :
    regex_listener_(0), period_(0), blocked_(true), thread_(0) {
  if (params.empty())
    return;

  ERS_DEBUG(0, "parameter = " << params[0]);

  std::istringstream in(params[0]);
  in >> period_;

  ERS_DEBUG(0, "delay between algorithms execution = " << period_);
}

dqm_core::test::DummyInput::~DummyInput() {
  delete thread_;
}

void dqm_core::test::DummyInput::simulator() {
  std::map<std::string, dqm_core::InputListener*>::iterator it =
      listeners_.begin();

  for (; it != listeners_.end(); ++it) {
    if (blocked_) {
      break;
    }

    ERS_DEBUG(1, "provide input for the '" << it->first << "' listener");
    it->second->handleInput(it->first,
        TH1D("Dummy Input Test", "Dummy Input Test", 10, 0.1, 1.1));

    if (regex_listener_) {
      if (boost::regex_match(it->first.c_str(), regex_)) {
        regex_listener_->handleInput(it->first,
            TH1D("Dummy Input Test", "Dummy Input Test", 10, 0.1, 1.1));
      }
    }
    sleep(period_);
  }
}

void dqm_core::test::DummyInput::addListener(const std::string & name,
    dqm_core::InputListener * listener) {
  ERS_DEBUG(1,
      "listener was registered with the '" << name << "' name" << period_);
  listeners_[name] = listener;
}

void dqm_core::test::DummyInput::addListener(const boost::regex & regex,
    dqm_core::InputListener * listener) {
  regex_ = regex;
  regex_listener_ = listener;
}

void dqm_core::test::DummyInput::addListener(
    const std::vector<std::string> & names,
    dqm_core::InputListener * listener) {
  for (size_t i = 0; i < names.size(); ++i) {
    listeners_[names[i]] = listener;
  }
}

void dqm_core::test::DummyInput::deactivate() {
  if (!thread_)
    return;

  blocked_ = true;
  thread_->join();
  delete thread_;
  thread_ = 0;
}

void dqm_core::test::DummyInput::activate() {
  if (thread_)
    return;

  blocked_ = false;
  thread_ = new std::thread(
      std::bind(&dqm_core::test::DummyInput::simulator, this));
}

void dqm_core::test::DummyInput::stop() {
  if (!thread_)
    return;

  blocked_ = true;
}

void dqm_core::test::DummyInput::wait() {
  if (!thread_)
    return;

  thread_->join();
  delete thread_;
  thread_ = 0;
}
