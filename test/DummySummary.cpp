/*! \file DummySummary.cxx file makes a DummySummary for the Parameters in the Region.  
 * \author Haleh Hadavand
 */

#include <dqm_core/AlgorithmRegistrator.h>

#include <dqm_core/test/DummySummary.h>

namespace {
  dqm_core::AlgorithmRegistrator<dqm_core::test::DummySummary> __sm__(
      "DummySummary");
}

dqm_core::SummaryMaker *
dqm_core::test::DummySummary::clone() {
  return new DummySummary();
}

dqm_core::Result *
dqm_core::test::DummySummary::execute(const std::string & name,
    const dqm_core::Result & result, const dqm_core::ParametersMap &) {

  ERS_DEBUG(1,
      "DummySummary executed for the '" << name
          << "' region and produces the following result: " << result);

  return result.clone();
}
