/* test_Alg.cpp
 Author Haleh Hadavand 06/12/06
 */

#include <iostream>

#include <boost/program_options.hpp>

#include <dqm_core/LibraryManager.h>
#include <dqm_core/AlgorithmManager.h>

#include <dqm_core/test/DummyAlgorithmConfig.h>

#include <TH1.h>
#include <ers/ers.h>

int main(int argc, char ** argv) {
  boost::program_options::options_description description("Options");

  description.add_options()("help,h", "produce help message")
      ("algorithm,a",
      boost::program_options::value<std::string>()->default_value(
          "DummyAlgorithm"), "name of algorithm to execute")
      ("library,l",
      boost::program_options::value<std::string>()->default_value(
          "libdqm_dummy.so"), "name of the algorithms library");

  boost::program_options::variables_map arguments;

  try {
    boost::program_options::store(
        boost::program_options::parse_command_line(argc, argv, description),
        arguments);
    boost::program_options::notify(arguments);
  }
  catch (boost::program_options::error & ex) {
    std::cerr << ex.what() << std::endl;
    description.print(std::cout);
    return 1;
  }

  if (arguments.count("help")) {
    std::cout << "Test application of the 'dqm_core' package" << std::endl;
    description.print(std::cout);
    return 0;
  }

  if (!arguments.count("algorithm")) {
    std::cerr << "'algorithm' option is not given" << std::endl;
    description.print(std::cout);
    return 2;
  }

  std::string algorithm_name = arguments["algorithm"].as<std::string>();
  std::string library_name = arguments["library"].as<std::string>();

  try {
    dqm_core::LibraryManager::instance().loadLibrary(library_name);
  }
  catch (dqm_core::Exception & ex) {
    ers::fatal(ex);
    return 3;
  }

  TH1F * histogram = new TH1F("histogram", "Simple Gaussian Histogram", 100, -2,
      2);
  histogram->FillRandom("gaus", 1000);

  try {
    dqm_core::Algorithm * algorithm =
        dqm_core::AlgorithmManager::instance().getAlgorithm(algorithm_name);
    dqm_core::test::DummyAlgorithmConfig config;
    dqm_core::Result * result = algorithm->clone()->execute("test", *histogram,
        config);
    std::cout << *result << std::endl;
    delete result;
  }
  catch (dqm_core::Exception & ex) {
    ers::fatal(ex);
    return 4;
  }

  return 0;
}

