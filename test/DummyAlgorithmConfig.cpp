/*! \file DummyAlgorithmConfig.cpp Implements the dqm_core::DummyAlgorithmConfig class.
 * \author Serguei Kolos
 * \version 1.0
 */

#include <TH1.h>
#include <map>
#include <iostream>
#include <dqm_core/test/DummyAlgorithmConfig.h>

TObject *
dqm_core::test::DummyAlgorithmConfig::getReference() const {
  return ref_;
}

void dqm_core::test::DummyAlgorithmConfig::setReference(TObject * ref) {
  ref_ = ref;
}

void dqm_core::test::DummyAlgorithmConfig::addGenericParameter(
    const std::string & name, const std::string & value) {
  m_generic_parameters.insert(std::make_pair(name, value));
}

void dqm_core::test::DummyAlgorithmConfig::addParameter(
    const std::string & name, double value) {
  m_parameters.insert(std::make_pair(name, value));
}

void dqm_core::test::DummyAlgorithmConfig::addGreenThreshold(
    const std::string & green, double value) {
  m_green_thresholds.insert(std::make_pair(green, value));
}

void dqm_core::test::DummyAlgorithmConfig::addRedThreshold(
    const std::string & red, double value) {
  m_red_thresholds.insert(std::make_pair(red, value));
}
