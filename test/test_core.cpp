/* test_core.cpp
 Author Serguei Kolos 15/09/06
 */

#include <boost/program_options.hpp>

#include <dqm_core/IOManager.h>
#include <dqm_core/LibraryManager.h>
#include <dqm_core/Region.h>
#include <dqm_core/Utils.h>

#include <dqm_core/test/DummyAlgorithmConfig.h>

int main(int argc, char ** argv) {
  boost::program_options::options_description description("Options");

  description.add_options()("help,h", "produce help message")
      ("Multi,M",
      "test parameter with multiple inputs")
      ("Propagate,P",
      "propagate disabled state to the children")
      ("Remote,R",
      "use remote region simulation")
      ("wait,w",
      boost::program_options::value<std::string>()->default_value("1"),
      "wait time between algorithms execution")
      ("output,o",
      boost::program_options::value<std::string>()->default_value("std::cout"),
      "where to put otput results")
      ("regex,E",
      boost::program_options::value<std::string>(),
      "regular expression for input object names")
      ("regions,r",
      boost::program_options::value<std::vector<std::string> >()->multitoken(),
      "names of DQM regions")
      ("parameters,p",
      boost::program_options::value<std::vector<std::string> >()->multitoken(),
      "names of DQM parameters")
      ("disabled,d",
      boost::program_options::value<std::vector<std::string> >()->multitoken(),
      "names of disbled DQM parameters/regions");

  boost::program_options::variables_map arguments;
  try {
    boost::program_options::store(
        boost::program_options::parse_command_line(argc, argv, description),
        arguments);
    boost::program_options::notify(arguments);
  }
  catch (boost::program_options::error & ex) {
    std::cerr << ex.what() << std::endl;
    description.print(std::cout);
    return 1;
  }

  if (arguments.count("help")) {
    std::cout << "Test application of the 'dqm_core' package" << std::endl;
    description.print(std::cout);
    return 0;
  }

  if (!arguments.count("parameters")) {
    std::cerr << "'parameters' option is not given" << std::endl;
    description.print(std::cout);
    return 2;
  }

  std::vector<std::string> parameters = arguments["parameters"].as<
      std::vector<std::string> >();
  std::vector<std::string> regions;
  std::vector<std::string> disabled;

  if (arguments.count("regions")) {
    regions = arguments["regions"].as<std::vector<std::string> >();
  }
  else {
    regions.push_back("R1");
    regions.push_back("R2");
  }

  if (arguments.count("disabled")) {
    disabled = arguments["disabled"].as<std::vector<std::string> >();
  }
  else {
    disabled.push_back("R1");
  }

  //////////////////////////////////////////////////////////////////////////////////////////////////
  // Uuu-f ... at last we have finished with parsing the command line
  //////////////////////////////////////////////////////////////////////////////////////////////////

  std::unique_ptr<dqm_core::Input> input;
  std::unique_ptr<dqm_core::Output> output;
  try {
    dqm_core::LibraryManager::instance().loadLibrary("libdqm_dummy_io.so");
    dqm_core::LibraryManager::instance().loadLibrary("libdqm_dummy.so");

    std::vector<std::string> input_params;
    input_params.push_back(arguments["wait"].as<std::string>());
    input.reset(
        dqm_core::IOManager::instance().createInput("DummyInput",
            input_params));

    std::vector<std::string> output_params;
    output_params.push_back(arguments["output"].as<std::string>());
    output.reset(
        dqm_core::IOManager::instance().createOutput("DummyOutput",
            output_params));
  }
  catch (dqm_core::Exception & ex) {
    ers::fatal(ex);
    return -1;
  }

  try {
    dqm_core::RegionConfig rconfig("DummySummary", 0.6);
    boost::shared_ptr<dqm_core::Region> root =
        dqm_core::Region::createRootRegion("root", *input.get(), *output.get(),
            rconfig);

    for (size_t n = 0; n < regions.size(); n++) {
      boost::shared_ptr<dqm_core::Region> current = root->addRegion(regions[n],
          rconfig);

      for (size_t i = 0; i < parameters.size(); i++) {
        std::string name = regions[n] + "_" + parameters[i];
        dqm_core::ParameterConfig pconfig(name, "DummyAlgorithm", 1,
            std::shared_ptr<dqm_core::AlgorithmConfig>(
                new dqm_core::test::DummyAlgorithmConfig));
        current->addParameter(name, pconfig);
      }
    }

    if (arguments.count("Multi")) {
      boost::shared_ptr<dqm_core::Region> multi_input = root->addRegion(
          "MultiInputRegion", rconfig);
      dqm_core::ParameterConfig pconfig("MultiInputParameter", "DummyAlgorithm",
          1,
          std::shared_ptr<dqm_core::AlgorithmConfig>(
              new dqm_core::test::DummyAlgorithmConfig));
      multi_input->addParameter("MultiInputParameter", pconfig);
    }

    if (arguments.count("Remote")) {
      root->addRemoteRegion("Remote", rconfig);
    }

    if (arguments.count("regex")) {
      boost::shared_ptr<dqm_core::Region> regex_region = root->addRegion(
          "RegexRegion", rconfig);
      dqm_core::ParameterConfig pconfig("RegexParameter", "DummyAlgorithm", 1,
          std::shared_ptr<dqm_core::AlgorithmConfig>(
              new dqm_core::test::DummyAlgorithmConfig));
      regex_region->addParameter("RegexParameter", pconfig);
    }

    std::cout << "Dumping configuration ..." << std::endl;
    root->print(std::cout);
    std::cout << std::endl;

    std::cout << "Testing full configuration ..." << std::endl;
    output->activate();
    input->activate();

    input->wait();
    std::cout << "Done" << std::endl << std::endl;

    std::cout << "Testing configuration with disabled regions ..." << std::endl;
    for (size_t i = 0; i < disabled.size(); i++) {
      std::cout << "Disabling '" << disabled[i] << "' parameter/region ... "
          << std::endl;
      bool status = dqm_core::disable(disabled[i], root,
          arguments.count("Propagate"));
      std::cout << (status ? "done" : "error") << std::endl;

      std::cout << "Disabling '" << disabled[i] << "' parameter/region ... "
          << std::endl;
      std::vector<boost::shared_ptr<dqm_core::Region> > v;
      v.push_back(root);
      status = dqm_core::disable(disabled[i], v.begin(), v.end(),
          arguments.count("Propagate"));
      std::cout << (status ? "done" : "error") << std::endl;
    }

    input->deactivate();
    input->activate();
    input->wait();
    std::cout << "Done" << std::endl << std::endl;

    std::cout << "Testing configuration with re-enabled regions ..."
        << std::endl;
    for (size_t i = 0; i < disabled.size(); i++) {
      std::cout << "Enabling '" << disabled[i] << "' parameter/region ... "
          << std::endl;
      bool status = dqm_core::enable(disabled[i], root,
          arguments.count("Propagate"));
      std::cout << (status ? "done" : "error") << std::endl;

      std::cout << "Enabling '" << disabled[i] << "' parameter/region ... "
          << std::endl;
      std::vector<boost::shared_ptr<dqm_core::Region> > v;
      v.push_back(root);
      status = dqm_core::enable(disabled[i], v.begin(), v.end(),
          arguments.count("Propagate"));
      std::cout << (status ? "done" : "error") << std::endl;

      std::cout << "Enabling 'root' parameter/region ... " << std::endl;
      status = dqm_core::enable("root", root, arguments.count("Propagate"));
      std::cout << (status ? "done" : "error") << std::endl;
    }

    input->deactivate();
    input->activate();
    input->wait();
    std::cout << "Done" << std::endl << std::endl;

    {
      std::cout << "Testing shared Regions/Parameters ..." << std::endl;
      boost::shared_ptr<dqm_core::Region> root1 =
          dqm_core::Region::createRootRegion("root1", *input.get(),
              *output.get(), rconfig);

      if (regions.size() > 1) {
        boost::shared_ptr<dqm_core::Node> p = root->find(regions[1]);

        if (p.get()) {
          root1->addChild(p);
        }

        boost::shared_ptr<dqm_core::Region> region = root1->addRegion(
            regions[0] + "1", rconfig);

        p = root->find(regions[0] + "_" + parameters[0]);

        if (p.get()) {
          region->addChild(p);
        }
      }

      input->deactivate();
      input->activate();
      input->wait();
      std::cout << "Done" << std::endl << std::endl;
    }

    std::cout << "Testing after destroying shared Regions/Parameters ..."
        << std::endl;
    input->deactivate();
    input->activate();
    input->wait();
    std::cout << "Done" << std::endl << std::endl;

    input->deactivate();
    output->deactivate();
  }
  catch (dqm_core::Exception & ex) {
    ers::fatal(ex);
    return -1;
  }
}

