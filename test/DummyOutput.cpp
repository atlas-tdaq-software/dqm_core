/*! \file DummyOutput.cpp Implements the dqm_core::test::DummyOutput class.
 * \author Serguei Kolos
 * \version 1.0
 */

#include <dqm_core/test/DummyOutput.h>
#include <dqm_core/OutputListener.h>
#include <dqm_core/Parameter.h>
#include <dqm_core/OutputFactoryT.h>

namespace {
  dqm_core::OutputFactoryT<dqm_core::test::DummyOutput> __factory__(
      "DummyOutput");
}

dqm_core::test::DummyOutput::DummyOutput(
    const std::vector<std::string> & params) :
    output_(&std::cout), active_(false) {
  if (params.empty())
    return;

  ERS_DEBUG(0, "parameter = " << params[0]);

  if (params[0] == "cerr")
    output_ = &std::cerr;
  else if (params[0] == "clog")
    output_ = &std::clog;
}

dqm_core::test::DummyOutput::~DummyOutput() {
}

void dqm_core::test::DummyOutput::publishResult(const std::string & name,
    const dqm_core::Result & result) {
  if (!active_)
    return;

  (*output_) << "DQ Result " << result << " has been produced for the '" << name
      << "' parameter/region" << std::endl;
}

void dqm_core::test::DummyOutput::addListener(const std::string &,
    dqm_core::OutputListener *) {
  // DummyOutput does not handle remote regions
}

void dqm_core::test::DummyOutput::deactivate() {
  active_ = false;
}

void dqm_core::test::DummyOutput::activate() {
  active_ = true;
}
