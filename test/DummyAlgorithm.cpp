/*! \file DummyAlgorithm.cxx compares bins of histogram wrt to reference histogram and counts number of bins N Sigma away from ref; returns dqm_core::Result
 * \author Haleh Hadavand
 */

#include <TH1.h>
#include <TF1.h>
#include <TObjArray.h>
#include <math.h>
#include <ers/ers.h>
#include <stdlib.h>

#include <dqm_core/AlgorithmConfig.h>
#include <dqm_core/AlgorithmRegistrator.h>

#include <dqm_core/test/DummyAlgorithm.h>

namespace {
  dqm_core::AlgorithmRegistrator<dqm_core::test::DummyAlgorithm> __aa__(
      "DummyAlgorithm");
}

dqm_core::Algorithm *
dqm_core::test::DummyAlgorithm::clone() {
  return new DummyAlgorithm();
}

dqm_core::Result *
dqm_core::test::DummyAlgorithm::execute(const std::string & name,
    const TObject &, const dqm_core::AlgorithmConfig & config) {
  ERS_DEBUG(1, "DummyAlgorithm Executed for the '" << name << "' parameter");

  const std::map<std::string, double> & parameters = config.getParameters();
  std::map<std::string, double>::const_iterator it = parameters.find(
      "min_status");
  int min_status = it != parameters.end() ? it->second : 0;

  it = parameters.find("max_status");
  int max_status = it != parameters.end() ? it->second : 3;

  it = parameters.find("status");
  int status =
      it != parameters.end() ?
          (int) it->second :
          ::rand() % (max_status - min_status + 1) + min_status;

  dqm_core::Result * result = new dqm_core::Result(
      dqm_core::Result::Status(status));

  it = parameters.find("histograms");
  int histograms = it != parameters.end() ? it->second : 0;
  result->tags_["min_status"] = min_status;
  result->tags_["max_status"] = max_status;
  result->tags_["histograms"] = histograms;

  if (histograms) {
    TObjArray * a = new TObjArray();
    a->SetOwner(true);
    for (int i = 0; i < 5; ++i) {
      std::ostringstream o;
      o << "histogram" << i;
      std::string n = o.str();
      TH1F * h = new TH1F(n.c_str(), "Simple Gaussian Histogram", 100, -2, 2);
      h->SetDirectory(0);
      h->FillRandom("gaus", 1000);
      a->Add(h);
    }

    result->object_.reset(a);
  }

  return result;
}

void dqm_core::test::DummyAlgorithm::printDescriptionTo(std::ostream & out) {
  out
      << "DummyAlgorithm: Dummy Algorithm to use in test applications in dqm_core\n"
      << std::endl;

  out << "Mandatory Parameter: \n" << std::endl;

  out << "Mandatory Green/Red Threshold: \n" << std::endl;

  out
      << "Optional Parameter: [min_stat] - minimum histogram statistics needed to perform this algorithm"
      << std::endl;
  out
      << "Optional Parameter: [status] - if given, the algorithm will produce results with this status only"
      << std::endl;
  out
      << "Optional Parameter: [min_status] - if given, the algorithm produce random results with the status more or equal to the [min_status] value"
      << std::endl;
  out
      << "Optional Parameter: [max_status] - if given, the algorithm produce random results with the status less or equal to the [max_status] value"
      << std::endl;
  out
      << "Optional Parameter: [histograms] - the number of histograms which algorithm will add to each result"
      << std::endl;
}

