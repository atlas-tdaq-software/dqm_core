#include <dqm_core/OutputMultiplexor.h>

dqm_core::OutputMultiplexor::OutputMultiplexor(dqm_core::Output * first,
    dqm_core::Output * second, dqm_core::Output * third) {
  if (first) {
    addOutput(first);
  }
  if (second) {
    addOutput(second);
  }
  if (third) {
    addOutput(third);
  }
}

void dqm_core::OutputMultiplexor::addOutput(dqm_core::Output * output) {
  output_.push_back(boost::shared_ptr<dqm_core::Output>(output));
}

void dqm_core::OutputMultiplexor::publishResult(const std::string & name,
    const dqm_core::Result & result) {
  for (size_t i = 0; i < output_.size(); ++i) {
    output_[i]->publishResult(name, result);
  }
}

void dqm_core::OutputMultiplexor::addListener(const std::string & name,
    dqm_core::OutputListener * listener) {
  for (size_t i = 0; i < output_.size(); ++i) {
    output_[i]->addListener(name, listener);
  }
}

void dqm_core::OutputMultiplexor::deactivate() {
  for (size_t i = 0; i < output_.size(); ++i) {
    output_[i]->deactivate();
  }
}

void dqm_core::OutputMultiplexor::activate() {
  for (size_t i = 0; i < output_.size(); ++i) {
    output_[i]->activate();
  }
}
