#include <dqm_core/InputMultiplexor.h>

dqm_core::InputMultiplexor::InputMultiplexor(dqm_core::Input * first,
    dqm_core::Input * second, dqm_core::Input * third) {
  if (first)
    addInput(first);
  if (second)
    addInput(second);
  if (third)
    addInput(third);
}

void dqm_core::InputMultiplexor::addInput(dqm_core::Input * input) {
  input_.push_back(boost::shared_ptr<dqm_core::Input>(input));
}

void dqm_core::InputMultiplexor::addListener(const std::string & name,
    dqm_core::InputListener * listener) {
  for (size_t i = 0; i < input_.size(); ++i) {
    input_[i]->addListener(name, listener);
  }
}

void dqm_core::InputMultiplexor::addListener(const boost::regex & regex,
    dqm_core::InputListener * listener) {
  for (size_t i = 0; i < input_.size(); ++i) {
    input_[i]->addListener(regex, listener);
  }
}

void dqm_core::InputMultiplexor::addListener(
    const std::vector<std::string> & names,
    dqm_core::InputListener * listener) {
  for (size_t i = 0; i < input_.size(); ++i) {
    input_[i]->addListener(names, listener);
  }
}

void dqm_core::InputMultiplexor::deactivate() {
  for (size_t i = 0; i < input_.size(); ++i) {
    input_[i]->deactivate();
  }
}

void dqm_core::InputMultiplexor::activate() {
  for (size_t i = 0; i < input_.size(); ++i) {
    input_[i]->activate();
  }
}

void dqm_core::InputMultiplexor::wait() {
  for (size_t i = 0; i < input_.size(); ++i) {
    input_[i]->wait();
  }
}

void dqm_core::InputMultiplexor::stop() {
  for (size_t i = 0; i < input_.size(); ++i) {
    input_[i]->stop();
  }
}
