/*! \file Region.cpp Implements the dqm_core::Region class.
 * \author Serguei Kolos
 * \version 1.0
 */
#include <unordered_map>

#include <boost/pointer_cast.hpp>

#include <dqm_core/AlgorithmManager.h>
#include <dqm_core/Output.h>
#include <dqm_core/Region.h>
#include <dqm_core/RegionProxy.h>
#include <dqm_core/TemplateParameter.h>
#include <dqm_core/Utils.h>

#include <ers/ers.h>

namespace {
  typedef std::unordered_map<std::string, boost::weak_ptr<dqm_core::Node>> Nodes;
  boost::mutex g_mutex;
  Nodes g_nodes;

  void add_node(const std::string & name,
      boost::shared_ptr<dqm_core::Node> ptr) {
    boost::mutex::scoped_lock lock(g_mutex);
    g_nodes[name] = ptr;
  }

  boost::shared_ptr<dqm_core::Node> find_node(const std::string & name) {
    boost::shared_ptr<dqm_core::Node> r;

    boost::mutex::scoped_lock lock(g_mutex);
    Nodes::iterator it = g_nodes.find(name);
    if (it != g_nodes.end()) {
      r = it->second.lock();
    }
    return r;
  }
}

boost::recursive_mutex dqm_core::Region::s_root_guard_;
dqm_core::Region::RootRegions dqm_core::Region::s_root_regions_;

boost::shared_ptr<dqm_core::Region> dqm_core::Region::createRootRegion(
    const std::string & name, Input & in, Output & out,
    const dqm_core::RegionConfig & config) {
  boost::recursive_mutex::scoped_lock lock(s_root_guard_);

  boost::shared_ptr<Region> r(
      new Region(name, in, out, boost::shared_ptr<Region>(), config),
      [](Node * n) {delete n;});

  s_root_regions_[name] = r;
  add_node(name, r);

  return r;
}

dqm_core::Region::Region(const std::string & name, Input & in, Output & out,
    boost::shared_ptr<Region> parent, const dqm_core::RegionConfig & config) :
    Node(name, out, parent, config), input_(in) {
  try {
    summary_maker_.reset(
        dqm_core::AlgorithmManager::instance().getSummaryMaker(
            config.algorithm_name_));
  }
  catch (dqm_core::SummaryMakerNotFound & ex) {
    throw dqm_core::BadConfig(ERS_HERE, name, "bad summary maker", ex);
  }
}

dqm_core::Region::~Region() {
  ERS_DEBUG(1, "destroy the '" << name_ << "' region");
}

boost::shared_ptr<dqm_core::Region> dqm_core::Region::self() {
  return boost::static_pointer_cast<Region>(shared_from_this());
}

void dqm_core::Region::addRemoteRegion(const std::string & name,
    const dqm_core::RegionConfig & config) {
  boost::recursive_mutex::scoped_lock lock(mutex_);

  dqm_core::ParametersMap::iterator it = children_.find(name);
  if (it != children_.end()) {
    throw dqm_core::AlreadyExist(ERS_HERE, name, name_);
  }

  boost::shared_ptr<Node> proxy = RegionProxy::create(name, output_, self(),
      config);

  children_[name] = proxy;
  add_node(name, proxy);

  ERS_DEBUG(1,
      "new proxy for remote region '" << name << "' has been added to the '"
          << name_ << "' region");
}

boost::shared_ptr<dqm_core::Region> dqm_core::Region::addRegion(
    const std::string & name, const dqm_core::RegionConfig & config) {
  {
    boost::recursive_mutex::scoped_lock lock(mutex_);

    dqm_core::ParametersMap::iterator it = children_.find(name);
    if (it != children_.end()) {
      throw dqm_core::AlreadyExist(ERS_HERE, name, name_);
    }
  }

  boost::recursive_mutex::scoped_lock lock(s_root_guard_);

  boost::shared_ptr<Node> node = find_node(name);

  if (node.get()) {
    addChild(node);
    return boost::dynamic_pointer_cast<dqm_core::Region, dqm_core::Node>(node);
  }

  boost::shared_ptr<Region> region(
      new dqm_core::Region(name, input_, output_, self(), config),
      [](Node * n) {delete n;});
  children_[name] = region;
  add_node(name, region);

  ERS_DEBUG(1,
      "new local region '" << name << "' has been added to the '" << name_
          << "' region");
  return region;
}

void dqm_core::Region::addChild(const boost::shared_ptr<Node> & node) {
  ERS_ASSERT(node.get());

  std::string name = node->getName();

  dqm_core::ParametersMap::iterator it = children_.find(name);
  if (it != children_.end()) {
    throw dqm_core::AlreadyExist(ERS_HERE, name, name_);
  }

  node->addParent(self());

  children_[name] = node;
  add_node(name, node);

  ERS_DEBUG(1,
      "new child '" << name << "' has been added to the '" << name_
          << "' region");
}

boost::shared_ptr<dqm_core::Parameter> dqm_core::Region::addParameter(
    const std::string & name, const dqm_core::ParameterConfig & config) {
  {
    boost::recursive_mutex::scoped_lock lock(mutex_);

    dqm_core::ParametersMap::iterator it = children_.find(name);
    if (it != children_.end()) {
      throw dqm_core::AlreadyExist(ERS_HERE, name, name_);
    }
  }

  boost::recursive_mutex::scoped_lock lock(s_root_guard_);

  boost::shared_ptr<Node> node = find_node(name);

  if (node.get()) {
    ERS_DEBUG(1,
        "node '" << name << "' already exists, adding it to the '" << name_
            << "' region");
    addChild(node);
    return boost::dynamic_pointer_cast<dqm_core::Parameter, dqm_core::Node>(
        node);
  }

  boost::shared_ptr<Parameter> parameter;
  if (config.input_is_regex_) {
    parameter = TemplateParameter::create(name, input_, output_, self(),
        config);
  }
  else {
    parameter = boost::shared_ptr<Parameter>(
        new Parameter(name, input_, output_, self(), config),
        [](Node * n) {delete n;});
  }

  children_[name] = parameter;
  add_node(name, parameter);

  ERS_DEBUG(1,
      "new node '" << name << "' has been added to the '" << name_
          << "' region");
  return parameter;
}

boost::shared_ptr<dqm_core::Parameter> dqm_core::Region::getParameter(
    const std::string & name, const dqm_core::ParameterConfig & config) {
  {
    boost::recursive_mutex::scoped_lock lock(mutex_);

    dqm_core::ParametersMap::iterator it = children_.find(name);
    if (it != children_.end()) {
      return boost::dynamic_pointer_cast<dqm_core::Parameter, dqm_core::Node>(
          it->second);
    }
  }

  boost::recursive_mutex::scoped_lock lock(s_root_guard_);

  boost::shared_ptr<Node> node = find_node(name);

  if (node.get()) {
    ERS_DEBUG(1,
        "node '" << name << "' already exists, adding it to the '" << name_
            << "' region");
    addChild(node);
    return boost::dynamic_pointer_cast<dqm_core::Parameter, dqm_core::Node>(
        node);
  }

  boost::shared_ptr<Parameter> parameter = boost::shared_ptr<Parameter>(
      new Parameter(name, output_, self(), config), [](Node * n) {delete n;});

  children_[name] = parameter;
  add_node(name, parameter);

  ERS_DEBUG(1,
      "new node '" << name << "' has been added to the '" << name_
          << "' region");
  return parameter;
}

void dqm_core::Region::handleResult(const std::string & name,
    const dqm_core::Result & result) {
  ERS_DEBUG(1,
      "result is available for the '" << name << "' node which belongs to the '"
          << name_ << "' region; result is " << result.status_);

  try {
    boost::shared_ptr<Result> local_result;
    {
      boost::recursive_mutex::scoped_lock lock(mutex_);

      if (result_->status_ == Result::Disabled) {
        ERS_DEBUG(1,
            "the '" << name_
                << "' region is disabled, summary maker will not be called");
        return;
      }

      result_.reset(summary_maker_->execute(name, result, children_));
      local_result = result_;
    }
    output_.publishResult(name_, *local_result);

    notifyParents(*local_result);
  }
  catch (Exception & ex) {
    ers::error(ex);
  }
}

void dqm_core::Region::stateChanged(bool enabled, bool propagate) {
  if (!enabled) {
    Node::stateChanged(enabled, propagate);
  }

  if (propagate) {
    boost::recursive_mutex::scoped_lock lock(mutex_);
    ParametersMap::iterator it = children_.begin();
    for (; it != children_.end(); ++it) {
      it->second->stateChanged(enabled, propagate);
    }
  }

  if (enabled) {
    Node::stateChanged(enabled, propagate);
  }
}

void dqm_core::Region::reset(bool notify_parents, bool propagate) {
  if (propagate) {
    boost::recursive_mutex::scoped_lock lock(mutex_);
    ParametersMap::iterator it = children_.begin();
    for (; it != children_.end(); ++it) {
      it->second->reset(notify_parents, propagate);
    }
  }

  Node::reset(notify_parents, propagate);
}

boost::shared_ptr<dqm_core::Node> dqm_core::Region::find(
    const std::string & name) {
  if (name_ == name) {
    return self();
  }

  boost::recursive_mutex::scoped_lock lock(mutex_);

  return dqm_core::find(name, children_.begin(), children_.end(),
      dqm_core::map_accessor);
}

void dqm_core::Region::print(std::ostream & out) const {
  int level = depth();

  for (int i = 0; i < level; ++i)
    out << "\t";
  out << "Region { ";
  Node::print(out);
  out << " contains " << children_.size() << " children:\n";
  ParametersMap::const_iterator it = children_.begin();
  for (; it != children_.end(); ++it) {
    it->second->print(out);
    out << "\n";
  }
  for (int i = 0; i < level; ++i)
    out << "\t";
  out << "}";
}
