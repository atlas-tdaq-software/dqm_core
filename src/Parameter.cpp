/*! \file Parameter.cpp contains the dqm_core::Parameter class implementation.
 * \author Serguei Kolos
 * \version 1.0
 */
#include <mutex>

#include <boost/spirit/include/karma.hpp>
#include <boost/fusion/adapted.hpp>

#include <dqm_core/AlgorithmManager.h>
#include <dqm_core/Parameter.h>
#include <dqm_core/Region.h>

#include <ers/ers.h>

dqm_core::Parameter::Parameter(const std::string & name, Input & in,
    Output & out, boost::shared_ptr<Region> parent,
    const dqm_core::ParameterConfig & config) :
    Node(name, out, parent, config),
    input_(&in),
    algorithm_config_(config.algorithm_config_),
    input_is_regex_(config.input_is_regex_),
    input_data_(config.input_data_) {
  try {
    algorithm_.reset(
        dqm_core::AlgorithmManager::instance().getAlgorithm(algorithm_name_));

    if (input_data_.size() == 0) {
      throw dqm_core::BadConfig(ERS_HERE, name, "Empty input source list");
    }

    if (enabled_) {
      subscribe();
    }
  }
  catch (dqm_core::Exception & ex) {
    throw dqm_core::BadConfig(ERS_HERE, name, ex.what(), ex);
  }
}

dqm_core::Parameter::Parameter(const std::string & name, Output & out,
    boost::shared_ptr<Region> parent, const dqm_core::ParameterConfig & config) :
    Node(name, out, parent, config),
    input_(0),
    algorithm_config_(config.algorithm_config_),
    input_is_regex_(config.input_is_regex_),
    input_data_(config.input_data_) {
  try {
    algorithm_.reset(
        dqm_core::AlgorithmManager::instance().getAlgorithm(algorithm_name_));

  }
  catch (dqm_core::Exception & ex) {
    throw dqm_core::BadConfig(ERS_HERE, name, ex.what(), ex);
  }
}

dqm_core::Parameter::~Parameter() {
  ERS_DEBUG(1, "destroy the '" << name_ << "' parameter");
}

void dqm_core::Parameter::subscribe() {
  if (!input_) {
    return;
  }

  if (input_data_.size() == 1) {
    input_->addListener(input_data_[0], this);
  }
  else {
    input_->addListener(input_data_, this);
  }
}

void dqm_core::Parameter::handleInput(const std::string & name,
    const TObject & object) {
  ERS_DEBUG(1,
      "input received for the " << name << "' object associated with the '"
          << name_ << "' parameter");

  if (!algorithm_config_->algorithmExecutionPermitted()) {
    ERS_DEBUG(1, "Algorithm execution is not permitted");
    return;
  }

  try {
    boost::shared_ptr<Result> local_result;
    {
      boost::mutex::scoped_lock lock(mutex_);

      if (result_->status_ == Result::Disabled) {
        ERS_DEBUG(1,
            "the '" << name_
                << "' parameter is disabled, algorithm will not be called");
        return;
      }

      ERS_DEBUG(1,
          "executing '" << algorithm_name_ << "' algorithm ' for the " << name
              << "' input object associated with the '" << name_
              << "' parameter");
      {
        boost::unique_lock<AlgorithmConfig> lock(*algorithm_config_);
        result_.reset(algorithm_->execute(name_, object, *algorithm_config_));
      }
      local_result = result_;
    }
    output_.publishResult(name_, *local_result);

    notifyParents(*local_result);
  }
  catch (dqm_core::HistStat & ex) {
    ers::log(ex);
  }
  catch (dqm_core::Exception & ex) {
    ers::error(ex);
  }
}

void dqm_core::Parameter::stateChanged(bool enabled, bool /* propagate */) {
  if (!enabled_ && enabled) {
    try {
      subscribe();
    }
    catch (dqm_core::Exception & ex) {
      ers::warning(ex);
    }
    enabled_ = enabled;
  }

  dqm_core::Node::stateChanged(enabled, false);
}

void dqm_core::Parameter::reset(bool notify_parents, bool /* propagate */) {
  boost::shared_ptr<Result> local_result;
  {
    boost::mutex::scoped_lock lock(mutex_);
    result_.reset(
        new Result(
            result_->status_ == Result::Disabled ?
                Result::Disabled : Result::Undefined));
    local_result = result_;
  }
  output_.publishResult(name_, *local_result);

  if (notify_parents) {
    notifyParents(*local_result);
  }
}

void dqm_core::Parameter::print(std::ostream & out) const {
  int level = depth();

  namespace karma = boost::spirit::karma;

  for (int i = 0; i < level; ++i)
    out << "\t";
  out << "Parameter { ";
  Node::print(out);
  out << "\n";

  for (int i = 0; i < level + 1; ++i)
    out << "\t";
  algorithm_config_->print(out);
  out << "\n";

  if (!input_data_.empty()) {
    for (int i = 0; i < level + 1; ++i)
      out << "\t";
    out << "input=[" << karma::format(karma::string % ", ", input_data_);
    out << "]\n";
  }

  for (int i = 0; i < level; ++i)
    out << "\t";
  out << "}";
}
