/*! \file AlgorithmManager.cpp Implements the dqm_core::AlgorithmManager class.
 * \author 
 * \version 1.0
 */

#include <dqm_core/AlgorithmManager.h>

boost::mutex dqm_core::AlgorithmManager::mutex_;
dqm_core::AlgorithmManager * dqm_core::AlgorithmManager::instance_;

dqm_core::AlgorithmManager::AlgorithmManager() {
  ;
}

dqm_core::AlgorithmManager &
dqm_core::AlgorithmManager::instance() {
  if (!instance_) {
    boost::mutex::scoped_lock lock(mutex_);
    if (!instance_) {
      //
      // Will never be destroyed but this does not harm
      //
      instance_ = new AlgorithmManager();
    }
  }
  return *instance_;
}

void dqm_core::AlgorithmManager::registerAlgorithm(const std::string & name,
    Algorithm * algorithm) {
  std::map<std::string, Algorithm*>::const_iterator it = algorithms_.find(name);
  if (it == algorithms_.end()) {
    algorithms_[name] = algorithm;
  }
}

void dqm_core::AlgorithmManager::registerAlgorithm(const std::string & name,
    SummaryMaker * summary) {
  std::map<std::string, SummaryMaker*>::const_iterator it = summaryMakers_.find(
      name);
  if (it == summaryMakers_.end()) {
    summaryMakers_[name] = summary;
  }
}

void dqm_core::AlgorithmManager::registerSummaryMaker(const std::string & name,
    SummaryMaker * summary) {
  registerAlgorithm(name, summary);
}

dqm_core::Algorithm *
dqm_core::AlgorithmManager::getAlgorithm(const std::string & name) const {
  std::map<std::string, Algorithm*>::const_iterator it = algorithms_.find(name);
  if (it != algorithms_.end()) {
    return ((*it).second)->clone();
  }
  else {
    throw dqm_core::AlgorithmNotFound(ERS_HERE, name);
  }
}

dqm_core::SummaryMaker *
dqm_core::AlgorithmManager::getSummaryMaker(const std::string & name) const {
  std::map<std::string, SummaryMaker*>::const_iterator it = summaryMakers_.find(
      name);
  if (it != summaryMakers_.end()) {
    return ((*it).second)->clone();
  }
  else {
    throw dqm_core::SummaryMakerNotFound(ERS_HERE, name);
  }
}
