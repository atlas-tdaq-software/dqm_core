/*! \file InputRootFile.cpp contains the dqm_core::InputRootFile class implementation.
 * \author Michael Wilson
 */

#include <dqm_core/InputRootFile.h>
#include <dqm_core/InputListener.h>
#include <dqm_core/InputFactoryT.h>

#include <TDirectory.h>
#include <TKey.h>
#include <TObject.h>

#include <ers/ers.h>

namespace {
  dqm_core::InputFactoryT<dqm_core::InputRootFile> __factory__("RootFileInput");
}

namespace dqm_core {

// *********************************************************************
// Public Methods
// *********************************************************************

  InputRootFile::InputRootFile(const std::string & rootFileName) :
      m_fileName(rootFileName), m_file(TFile::Open(rootFileName.c_str())) {
    if (m_file.get() == 0) {
      throw dqm_core::FileNotReadable(ERS_HERE, rootFileName);
    }
  }

  InputRootFile::InputRootFile(const std::vector<std::string> & parameters) :
      m_fileName(parameters.size() ? parameters[0] : std::string()), m_file(
          TFile::Open(m_fileName.c_str())) {
    if (m_file.get() == 0) {
      throw dqm_core::FileNotReadable(ERS_HERE, m_fileName);
    }
  }

  InputRootFile::~InputRootFile() {
  }

  void InputRootFile::addListener(const std::string & name,
      InputListener * listener) {
    if (m_file.get() == 0) {
      throw dqm_core::FileNotReadable(ERS_HERE, m_fileName);
    }

    // TKeys are owned by the TFile
    TKey* key = getObjKey(m_allDirs, m_file.get(), "", name);
    if (key == 0) {
      throw dqm_core::MonObjectNotFound(ERS_HERE, name);
    }

    m_monObjects.push_back(MonBundle(name, key, listener));
  }

  void InputRootFile::addListener(const boost::regex &, InputListener *) {
    throw dqm_core::Exception(ERS_HERE,
        "This method is intentionally not implemented");
  }

  void InputRootFile::addListener(const std::vector<std::string> &,
      InputListener *) {
    throw dqm_core::Exception(ERS_HERE,
        "This method is intentionally not implemented");
  }

  void InputRootFile::activate() {
    MonContainerIter monEnd = m_monObjects.end();
    for (MonContainerIter i = m_monObjects.begin(); i != monEnd; ++i) {
      TObject* obj = i->key->ReadObj();
      i->listener->handleInput(i->pathAndName, *obj);
      delete obj;
    }
  }

  void InputRootFile::deactivate() {
  }

  void InputRootFile::stop() {
  }

  void InputRootFile::wait() {
  }

// *********************************************************************
// Protected Methods
// *********************************************************************

  TKey*
  InputRootFile::getObjKey(TDirectory* dir, std::string path) {
    if (dir == 0)
      return 0;

    TKey* key(0);

    std::string::size_type i = path.find_first_of('/');
    if (i != std::string::npos) {
      std::string dName(path, 0, i);
      std::string pName(path, i + 1, std::string::npos);
      if (dName != "") {
        key = dir->FindKey(dName.c_str());
        if (key != 0) {
          TDirectory* subDir = dynamic_cast<TDirectory*>(key->ReadObj());
          return getObjKey(subDir, pName);
        }
        return 0;
      }
      return getObjKey(dir, pName);
    }

    return dir->FindKey(path.c_str());
  }

  TKey*
  InputRootFile::getObjKey(DirMap_t& dirmap, TDirectory* dir,
      std::string leadingPath, std::string path) {
    if (dir == 0)
      return 0;

    TKey* key(0);

    if (leadingPath == "") {
      std::string::size_type j = path.find_last_of('/');
      if (j != std::string::npos) {
        std::string dName(path, 0, j);
        std::string pName(path, j + 1, std::string::npos);
        DirMap_t::const_iterator diri = dirmap.find(dName);
        if (diri != dirmap.end()) {
          TDirectory* d = diri->second;
          return d->FindKey(pName.c_str());
        }
      }
    }

    std::string::size_type i = path.find_first_of('/');
    if (i != std::string::npos) {
      std::string dName(path, 0, i);
      std::string pName(path, i + 1, std::string::npos);
      if (dName != "") {
        key = dir->FindKey(dName.c_str());
        if (key != 0) {
          TDirectory* subDir = dynamic_cast<TDirectory*>(key->ReadObj());
          std::string subLeadingPath(leadingPath);
          if (leadingPath != "")
            subLeadingPath += "/";
          subLeadingPath += dName;
          DirMap_t::value_type dirmapVal(subLeadingPath, subDir);
          dirmap.insert(dirmapVal);
          return getObjKey(dirmap, subDir, subLeadingPath, pName);
        }
        return 0;
      }
      return getObjKey(dirmap, dir, leadingPath, pName);
    }

    return dir->FindKey(path.c_str());
  }

// *********************************************************************
// Private Methods
// *********************************************************************

  InputRootFile::InputRootFile() {
  }

} // namespace dqm_core
