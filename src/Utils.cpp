/*! \file Utils.cpp Implements the dqm_core helper functions.
 * \author Serguei Kolos
 * \version 1.0
 */

#include <dqm_core/Utils.h>
#include <dqm_core/Region.h>

namespace dqm_core {
  template<class Iterator>
  boost::shared_ptr<Node> map_weak_accessor(Iterator it) {
    return it->second.lock();
  }
}

boost::shared_ptr<dqm_core::Node> dqm_core::find(const std::string & name) {
  boost::recursive_mutex::scoped_lock lock(Region::s_root_guard_);

  return find(name, Region::s_root_regions_.begin(),
      Region::s_root_regions_.end(), map_weak_accessor);
}

bool dqm_core::enable(const std::string & name, boost::shared_ptr<Node> root,
    bool propagate) {
  boost::shared_ptr<Node> p = root->find(name);
  if (p.get()) {
    p->stateChanged(true, propagate);
    return true;
  }
  return false;
}

bool dqm_core::disable(const std::string & name, boost::shared_ptr<Node> root,
    bool propagate) {
  boost::shared_ptr<Node> p = root->find(name);
  if (p.get()) {
    p->stateChanged(false, propagate);
    return true;
  }
  return false;
}
