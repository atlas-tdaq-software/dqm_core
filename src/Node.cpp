/*! \file Node.cpp contains the dqm_core::Node class implementation.
 * \author Serguei Kolos
 * \version 1.0
 */

#include <dqm_core/AlgorithmManager.h>
#include <dqm_core/Node.h>
#include <dqm_core/Region.h>

#include <ers/ers.h>

dqm_core::Node::Node(const std::string & name, Output & out,
    boost::shared_ptr<Region> parent, const NodeConfig & config) :
    name_(name),
    algorithm_name_(config.algorithm_name_),
    weight_(config.weight_),
    enabled_(config.enabled_),
    output_(out),
    result_(new Result(config.enabled_ ? Result::Undefined : Result::Disabled)) {
  if (parent.get()) {
    parents_.push_back(parent);
  }
}

dqm_core::Node::~Node() {
  ERS_DEBUG(1, "destroy the '" << name_ << "' parameter");
}

boost::shared_ptr<dqm_core::Result> dqm_core::Node::getResult() const {
  boost::mutex::scoped_lock lock(mutex_);
  return result_;
}

void dqm_core::Node::addParent(const boost::shared_ptr<Region> & parent) {
  parents_.push_back(parent);
}

void dqm_core::Node::notifyParents(const Result & result) {
  for (ParentsList::iterator it = parents_.begin(); it != parents_.end();
      ++it) {
    boost::shared_ptr<Region> r = it->lock();
    if (r.get()) {
      static_cast<OutputListener*>(r.get())->handleResult(name_, result);
    }
  }
}

void dqm_core::Node::stateChanged(bool enabled, bool /* propagate */) {
  boost::shared_ptr<Result> local_result;
  {
    boost::mutex::scoped_lock lock(mutex_);
    result_.reset(new Result(enabled ? Result::Undefined : Result::Disabled));
    local_result = result_;
  }
  output_.publishResult(name_, *local_result);

  notifyParents(*local_result);
}

void dqm_core::Node::reset(bool notify_parents, bool /* propagate */) {
  boost::shared_ptr<Result> local_result;
  {
    boost::mutex::scoped_lock lock(mutex_);
    result_.reset(
        new Result(
            result_->status_ == Result::Disabled ?
                Result::Disabled : Result::Undefined));
    local_result = result_;
  }
  output_.publishResult(name_, *local_result);

  if (notify_parents) {
    notifyParents(*local_result);
  }
}

boost::shared_ptr<dqm_core::Node> dqm_core::Node::find(
    const std::string & name) {
  return (name_ == name ? shared_from_this() : boost::shared_ptr<Node>());
}

int dqm_core::Node::depth() const {
  const Node * r = this;
  int d = 0;
  while (!r->parents_.empty()) {
    ++d;
    r = r->parents_[0].lock().get();
  }
  return d;
}

void dqm_core::Node::print(std::ostream & out) const {
  out << "name='" << name_ << "' weight=" << weight_ << " algorithm='"
      << algorithm_name_ << "'";
}
