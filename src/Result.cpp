/*! \file Result.cpp Implements the dqm_core::Result class.
 * \author Serguei Kolos
 * \version 1.0
 */

#include <boost/date_time/c_local_time_adjustor.hpp>

#include <dqm_core/Result.h>

#include <ers/ers.h>

TObject *
dqm_core::Result::getObject() const {
  return object_.get();
}

std::string dqm_core::Result::getTimeString() const {
  return boost::posix_time::to_simple_string(
      boost::date_time::c_local_adjustor<boost::posix_time::ptime>::utc_to_local(
          universal_time_));
}

boost::posix_time::ptime dqm_core::Result::getLocalTime() const {
  return boost::date_time::c_local_adjustor<boost::posix_time::ptime>::utc_to_local(
      universal_time_);
}

dqm_core::Result *
dqm_core::Result::clone() const {
  Result * result = new Result(status_, 0, universal_time_);
  result->tags_ = tags_;
  result->object_ = object_;
  return result;
}

std::ostream &
operator<<(std::ostream & out, dqm_core::Result::Status status) {
  switch (status) {
    case dqm_core::Result::Disabled:
      out << "'Disabled'";
      break;
    case dqm_core::Result::Undefined:
      out << "'Undefined'";
      break;
    case dqm_core::Result::Red:
      out << "'Red'";
      break;
    case dqm_core::Result::Green:
      out << "'Green'";
      break;
    case dqm_core::Result::Yellow:
      out << "'Yellow'";
      break;
    default:
      out << "ERROR:: Invalid dqm_core::Result::Status value '" << (long) status
          << "'";
      break;
  }
  return out;
}

std::ostream &
operator<<(std::ostream & out, const dqm_core::Result & result) {
  out << "{ " << result.status_;

  for (std::map<std::string, double>::const_iterator it = result.tags_.begin();
      it != result.tags_.end(); ++it) {
    out << ", [" << it->first << ":" << it->second << "]";
  }
  if (result.object_.get()) {
    out << ", [TObject:" << result.object_->GetName() << "]";
  }
  out << " } produced at " << result.getTimeString();
  return out;
}
