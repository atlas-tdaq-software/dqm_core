/*! \file RegionProxy.cpp Implements the dqm_core::RegionProxy class.
 * \author Serguei Kolos
 * \version 1.0
 */

#include <dqm_core/Output.h>
#include <dqm_core/Region.h>
#include <dqm_core/RegionProxy.h>

#include <ers/ers.h>

boost::shared_ptr<dqm_core::RegionProxy> dqm_core::RegionProxy::create(
    const std::string & name, Output & out, boost::shared_ptr<Region> parent,
    const dqm_core::RegionConfig & config) {
  return boost::shared_ptr<RegionProxy>(
      new RegionProxy(name, out, parent, config), [](Node * n) {delete n;});
}

dqm_core::RegionProxy::RegionProxy(const std::string & name, Output & out,
    boost::shared_ptr<Region> parent, const dqm_core::RegionConfig & config) :
    Node(name, out, parent, config) {
  try {
    output_.addListener(name, this);
  }
  catch (Exception & ex) {
    throw dqm_core::BadConfig(ERS_HERE, name, ex.what(), ex);
  }
}

dqm_core::RegionProxy::~RegionProxy() {
  ERS_DEBUG(1, "destroy the '" << name_ << "' proxy region");
}

void dqm_core::RegionProxy::handleResult(const std::string &,
    const dqm_core::Result & result) {
  ERS_DEBUG(1,
      "proxy for the '" << name_
          << "' remote region has received the DQ result");

  try {
    boost::shared_ptr<Result> local_result;
    {
      boost::mutex::scoped_lock lock(mutex_);

      if (result_->status_ == Result::Disabled) {
        ERS_DEBUG(1,
            "the '" << name_
                << "' region is disabled, remote result will not be propagated");
        return;
      }

      result_.reset(result.clone());
      local_result = result_;
    }

    notifyParents(*local_result);
  }
  catch (Exception & ex) {
    ers::error(ex);
  }
}
