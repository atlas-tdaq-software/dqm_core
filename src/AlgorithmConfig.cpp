#include <dqm_core/AlgorithmConfig.h>
#include <boost/spirit/include/karma.hpp>
#include <boost/fusion/adapted.hpp>

namespace karma = boost::spirit::karma;

void dqm_core::AlgorithmConfig::print(std::ostream & out) const {
  const std::map<std::string, std::string> & gp = getGenericParameters();
  if (!gp.empty())
    out << "generic_parameters=["
        << karma::format((karma::string << '=' << karma::string) % ", ", gp)
        << "] ";

  const std::map<std::string, double> & p = getParameters();
  if (!p.empty())
    out << "parameters=["
        << karma::format((karma::string << '=' << karma::double_) % ", ", p)
        << "] ";

  const std::map<std::string, double> & rh = getRedThresholds();
  if (!rh.empty())
    out << "red_thresholds=["
        << karma::format((karma::string << '=' << karma::double_) % ", ", rh)
        << "] ";

  const std::map<std::string, double> & gh = getGreenThresholds();
  if (!gh.empty())
    out << "green_thresholds=["
        << karma::format((karma::string << '=' << karma::double_) % ", ", gh)
        << "]";
}
