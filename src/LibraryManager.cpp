#include <dqm_core/LibraryManager.h>
#include <dlfcn.h>

/** This method returns the singleton instance. 
 * It should be used for every operation on the stream manager.
 * \return a reference to the singleton instance
 */
dqm_core::LibraryManager &
dqm_core::LibraryManager::instance() {
  static dqm_core::LibraryManager * instance = ers::SingletonCreator
      < dqm_core::LibraryManager > ::create();

  return *instance;
}

/** Private constructor - can not be called by user code, use the \c instance() method instead
 * \see instance()
 */
dqm_core::LibraryManager::LibraryManager() {
}

dqm_core::LibraryManager::SharedLibrary::SharedLibrary(
    const std::string & name) {
  m_handle = dlopen(name.c_str(), RTLD_LAZY | RTLD_GLOBAL);

  if (!m_handle) {
    throw CantLoadLibrary(ERS_HERE, name, dlerror());
  }
}

dqm_core::LibraryManager::SharedLibrary::~SharedLibrary() {
  ////////////////////////////////////////////////////////////////////
  // Library should be unloaded, but with some compilers (e.g gcc323)
  // this may result in crash of program, because it does not recognize
  // situation when the library is still in use
  ////////////////////////////////////////////////////////////////////

  //dlclose( m_handle );
}

dqm_core::LibraryManager::~LibraryManager() {
}

void dqm_core::LibraryManager::loadLibrary(const std::string & name) {
  boost::mutex::scoped_lock lock(m_mutex);
  LibMap::iterator it = m_libraries.find(name);

  if (it != m_libraries.end()) {
    ERS_DEBUG(1, "Library '" << name << "' is already loaded");
    return;
  }

  m_libraries.insert(
      std::make_pair(name,
          boost::shared_ptr<SharedLibrary>(new SharedLibrary(name))));
}
