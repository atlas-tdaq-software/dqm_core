/*
 *  IOManager.cxx
 *  dqm_core
 *
 *  Created by Serguei Kolos on 12.09.08.
 *  Copyright 2005 CERN. All rights reserved.
 *
 */

#include <dqm_core/LibraryManager.h>
#include <dqm_core/IOManager.h>
#include <dqm_core/InputFactory.h>
#include <dqm_core/OutputFactory.h>

/** This method returns the singleton instance. 
 * It should be used for every operation on the stream manager.
 * \return a reference to the singleton instance
 */
dqm_core::IOManager &
dqm_core::IOManager::instance() {
  static dqm_core::IOManager * instance = ers::SingletonCreator
      < dqm_core::IOManager > ::create();

  return *instance;
} // instance

/** Private constructor - can not be called by user code, use the \c instance() method instead
 * \see instance()
 */
dqm_core::IOManager::IOManager() {
}

/** Destructor - basic cleanup
 */
dqm_core::IOManager::~IOManager() {
}

dqm_core::Input *
dqm_core::IOManager::createInput(const std::string & name,
    const std::vector<std::string> & params) {
  boost::mutex::scoped_lock lock(m_mutex);
  InputFactories::const_iterator it = m_input_factories.find(name);
  if (it != m_input_factories.end())
    return it->second->createInput(params);

  throw dqm_core::FactoryNotFound(ERS_HERE, "input", name);
}

dqm_core::Output *
dqm_core::IOManager::createOutput(const std::string & name,
    const std::vector<std::string> & params) {
  boost::mutex::scoped_lock lock(m_mutex);
  OutputFactories::const_iterator it = m_output_factories.find(name);
  if (it != m_output_factories.end())
    return it->second->createOutput(params);

  throw dqm_core::FactoryNotFound(ERS_HERE, "output", name);
}

void dqm_core::IOManager::registerInputFactory(const std::string & name,
    dqm_core::InputFactory * factory) {
  boost::mutex::scoped_lock lock(m_mutex);
  InputFactories::iterator it = m_input_factories.find(name);
  if (it != m_input_factories.end())
    throw dqm_core::FactoryAlreadyRegistered(ERS_HERE, "input", name);

  m_input_factories.insert(
      std::make_pair(name, boost::shared_ptr<dqm_core::InputFactory>(factory)));
}

void dqm_core::IOManager::registerOutputFactory(const std::string & name,
    dqm_core::OutputFactory * factory) {
  boost::mutex::scoped_lock lock(m_mutex);
  OutputFactories::iterator it = m_output_factories.find(name);
  if (it != m_output_factories.end())
    throw dqm_core::FactoryAlreadyRegistered(ERS_HERE, "output", name);

  m_output_factories.insert(
      std::make_pair(name,
          boost::shared_ptr<dqm_core::OutputFactory>(factory)));
}
