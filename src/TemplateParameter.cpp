/*! \file Parameter.cpp contains the dqm_core::TemplateParameter class implementation.
 * \author Serguei Kolos
 * \version 1.0
 */

#include <dqm_core/AlgorithmManager.h>
#include <dqm_core/TemplateParameter.h>
#include <dqm_core/Region.h>

#include <ers/ers.h>

boost::shared_ptr<dqm_core::TemplateParameter> dqm_core::TemplateParameter::create(
    const std::string & name, Input & in, Output & out,
    boost::shared_ptr<Region> parent,
    const dqm_core::ParameterConfig & config) {
  return boost::shared_ptr<TemplateParameter>(
      new TemplateParameter(name, in, out, parent, config),
      [](Node * n) {delete n;});
}

dqm_core::TemplateParameter::TemplateParameter(const std::string & name,
    Input & in, Output & out, boost::shared_ptr<Region> parent,
    const dqm_core::ParameterConfig & config) :
    Parameter(name, out, parent, config), pconfig_(config) {
  try {
    if (config.input_data_.size() == 1) {
      regex_ = boost::regex(config.input_data_[0]);
      in.addListener(regex_, this);
    }
    else {
      throw dqm_core::BadConfig(ERS_HERE, name,
          "Multiple input sources are not allowed for template parameter");
    }
  }
  catch (dqm_core::Exception & ex) {
    throw dqm_core::BadConfig(ERS_HERE, name, ex.what(), ex);
  }
}

dqm_core::TemplateParameter::~TemplateParameter() {
  ERS_DEBUG(1, "destroy the '" << name_ << "' template parameter");
}

void dqm_core::TemplateParameter::handleInput(const std::string & name,
    const TObject & object) {
  {
    boost::mutex::scoped_lock lock(mutex_);

    if (result_->status_ == Result::Disabled) {
      ERS_DEBUG(1,
          "the '" << name_
              << "' template parameter is disabled, input will not be processed ");
      return;
    }
  }

  boost::smatch matches;
  if (!boost::regex_match(name, matches, regex_)) {
    throw dqm_core::RegexMatchFailed(ERS_HERE, name, regex_.str());
  }

  std::string parameter_name = name_; // use Parameter name

  ////////////////////////////////////////////////////////////////////////
  // matches[0] contains the original string.	 matches[n]
  // contains a sub_match object for each matching subexpression
  ////////////////////////////////////////////////////////////////////////
  size_t begin = matches.size() > 1 ? 1 : 0;
  for (size_t i = begin; i < matches.size(); i++) {
    ////////////////////////////////////////////////////////////////////////
    // sub_match::first and sub_match::second are iterators that refer to
    // the first and one past the last chars of the matching subexpression
    ////////////////////////////////////////////////////////////////////////
    parameter_name += "_" + std::string(matches[i].first, matches[i].second);
  }

  ERS_DEBUG(1,
      "executing '" << algorithm_name_ << "' algorithm ' for the '" << name
          << "' input object associated with the '" << parameter_name
          << "' parameter");

  for (ParentsList::iterator it = parents_.begin(); it != parents_.end();
      ++it) {
    boost::shared_ptr<Region> r = it->lock();
    if (r.get()) {
      boost::shared_ptr<Parameter> parameter = r->getParameter(parameter_name,
          pconfig_);
      static_cast<InputListener*>(parameter.get())->handleInput(name, object);
    }
  }
}
