#ifndef _DQM_CORE_INPUT_FACTORY_H_
#define _DQM_CORE_INPUT_FACTORY_H_

/*! \file InputFactory.h Declares the dqm_core::InputFactory interface.
 * \author Serguei Kolos
 * \version 1.0
 */

#include <dqm_core/exceptions.h>

/*! \namespace dqm_core
 *  This is a wrapping namespace for all public classes of the Data Quality Monitoring framework.
 */
namespace dqm_core {
  class Input;

  /*! \brief This class defines abstract interface for creating dqm_core::Input objects.
   \ingroup public
   */
  struct InputFactory {
    /*! Virtual destructor is defined to make sure that destructor of the sub-class will be
     called whenever the delete operator is applied to the dqm_core::InputFactory pointer.
     */
    virtual ~InputFactory() {
      ;
    }

    /*! This function is called to create a new Input object.
     \param params parameters' for the new input stream
     */
    virtual Input * createInput(const std::vector<std::string> & params) = 0;

  };
}

#endif
