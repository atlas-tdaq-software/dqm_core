#ifndef _DQM_CORE_OUTPUT_MULTIPLEXOR_H_
#define _DQM_CORE_OUTPUT_MULTIPLEXOR_H_

/*! \file OutputMultiplexor.h Declares the dqm_core::OutputMultiplexor class.
 * \author Serguei Kolos
 * \version 1.0
 */

#include <boost/shared_ptr.hpp>

#include <dqm_core/Output.h>

/*! \namespace dqm_core
 *  This is a wrapping namespace for all public classes of the Data Quality Monitoring framework.
 */
namespace dqm_core {
  class OutputMultiplexor: public dqm_core::Output {

  public:

    OutputMultiplexor(dqm_core::Output * first = 0, dqm_core::Output * second =
        0, dqm_core::Output * third = 0);

    /*! Add nested stream. The multimplexor object will become an owner
     *  of this stream and will delete it in its destructor.
     */
    void addOutput(dqm_core::Output * output);

  private:
    /*! Calls this function for all nested streams.
     */
    void publishResult(const std::string & name,
        const dqm_core::Result & result);

    /*! Calls this function for all nested streams.
     */
    void addListener(const std::string & name,
        dqm_core::OutputListener * listener);

    /*! Calls this function for all nested streams.
     */
    void activate();

    /*! Calls this function for all nested streams.
     */
    void deactivate();

  private:
    std::vector<boost::shared_ptr<dqm_core::Output>> output_;
  };
}

#endif
