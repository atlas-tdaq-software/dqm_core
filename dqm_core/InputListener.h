#ifndef _DQM_CORE_INPUT_LISTENER_H_
#define _DQM_CORE_INPUT_LISTENER_H_

/*! \file InputListener.h Declares the dqm_core::InputListener class.
 * \author Serguei Kolos
 * \version 1.0
 */

#include <string>

#include <dqm_core/Input.h>

class TObject;

/*! \namespace dqm_core
 *  This is a wrapping namespace for all public classes of the Data Quality Monitoring framework.
 */
namespace dqm_core {
  /*! \brief This class defines abstract interface for the notification of availability
   of the information for the Data Quality assessment. User has to declare a
   class which implements this interface and register instances of such class
   with the dqm_core::Input class via one of the dqm_core::Input::addListener functions.
   \ingroup public
   \sa	dqm_core::Input
   */
  struct InputListener {
    /*! Virtual destructor is defined to make sure that destructor of the sub-class will be
     called whenever the delete operator is applied to the dqm_core::InputListener pointer.
     */
    virtual ~InputListener() {
      ;
    }

    /*! This function is called whenever an object for which this listener has been
     registered appears in the corresponding dqm_core::Input stream.
     \param name objects's name
     \param object Root object which have to be used for DQ assessment
     */
    virtual void handleInput(const std::string & name,
        const TObject & object) = 0;
  };
}

#endif
