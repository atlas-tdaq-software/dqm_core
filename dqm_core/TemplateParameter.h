#ifndef _DQM_CORE_TEMPLATE_PARAMETER_H_
#define _DQM_CORE_TEMPLATE_PARAMETER_H_

/*! \file TemplateParameter.h Declares the dqm_core::TemplateParameter class.
 * \author Serguei Kolos
 * \version 1.0
 */

#include <dqm_core/Parameter.h>

/*! \namespace dqm_core
 *  This is a wrapping namespace for all public classes of the Data Quality Monitoring framework.
 */
namespace dqm_core {
  class Region;

  class TemplateParameter: public Parameter {
  public:
    static boost::shared_ptr<TemplateParameter> create(const std::string & name,
        Input & in, Output & out, boost::shared_ptr<Region> parent,
        const dqm_core::ParameterConfig & config);

  private:
    TemplateParameter(const std::string & name, Input & in, Output & out,
        boost::shared_ptr<Region> parent,
        const dqm_core::ParameterConfig & config);

    ~TemplateParameter();

    void handleInput(const std::string & name, const TObject & object);

    ParameterConfig pconfig_;
    boost::regex regex_;
  };
}

#endif
