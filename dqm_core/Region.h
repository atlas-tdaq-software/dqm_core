#ifndef _DQM_CORE_REGION_H_
#define _DQM_CORE_REGION_H_

/*! \file Region.h Declares the dqm_core::Region class.
 * \author Serguei Kolos
 * \version 1.0
 */
#include <map>

#include <boost/thread/recursive_mutex.hpp>

#include <dqm_core/exceptions.h>
#include <dqm_core/Node.h>
#include <dqm_core/OutputListener.h>
#include <dqm_core/Parameter.h>
#include <dqm_core/ParameterConfig.h>
#include <dqm_core/RegionConfig.h>
#include <dqm_core/Result.h>
#include <dqm_core/SummaryMaker.h>

/*! \namespace dqm_core
 *  This is a wrapping namespace for all public classes of the Data Quality Monitoring framework.
 */
namespace dqm_core {
  class RegionProxy;
  class TemplateParameter;

  class Region: public OutputListener, public Node {
    friend class RegionProxy;
    friend class TemplateParameter;
    friend boost::shared_ptr<dqm_core::Node> find(const std::string &);

  public:
    /*! This function returns all children of this region.
     */
    ParametersMap & children() {
      return children_;
    }

    /*! Always returns false.
     */
    bool isLeaf() const {
      return false;
    }

    /*! Creates new Region which can be used as root node for a new DQ hierarchy.
     */
    static boost::shared_ptr<Region> createRootRegion(const std::string & name,
        Input & in, Output & out, const dqm_core::RegionConfig & config);

    /*! Creates new Region and adds it to this Region.
     */
    boost::shared_ptr<Region> addRegion(const std::string & name,
        const dqm_core::RegionConfig & config);

    /*! Creates new Region which has to be a local representative of the real
     region living in another memory space (i.e. another process) and adds
     it to this Region.
     */
    void addRemoteRegion(const std::string & name,
        const dqm_core::RegionConfig & config);

    /*! Creates new Parameter and adds it to this Region.
     */
    boost::shared_ptr<Parameter> addParameter(const std::string & name,
        const dqm_core::ParameterConfig & config);

    /*! Add already existing Parameter to this Region.
     */
    void addChild(const boost::shared_ptr<Node> & node);

    /*! This function can be used to get a pointer to Region or Parameter object
     which belongs to a branch of the current node and is associated with the given name.
     If the current node has the given name then the function returns <b>this</b> pointer.
     \param name parameter name
     \return 	empty shared pointer if there is no region/paramater associated with the given name,
     otherwise a pointer to the target object
     */
    boost::shared_ptr<Node> find(const std::string & name);

    /*! This function resets the result of this Region to the Undefined state.
     If the Result of this Region has Disabled status then it will not be reset.
     \param notify_parents 	if the value is <b>true</b> then will call the handleResult on the parent Regions
     \param propagate 		<b>false</b> will reset result only for this region, <b>true</b> will reset the whole branch
     */
    void reset(bool notify_parents, bool propagate);

    /*! This function can be used to enable/disable the current Region. A Region is always
     constructed with enabled state. If the Region is disabled then its status is set to
     dqm_core::Result::Disabled and the SummaryMaker associated with it will not be called
     until it is re-enabled again. If the Region is enabled its state will be set to
     dqm_core::Result::Unknown and the SummaryMaker will be called as soon as one of
     the regions children changes its state.
     There are two modes of disabling a Region. If the <i>propagate</i> parameter is set to
     <b>false</b>, then all the children of the Region will remain enabled and their statuses
     will still be calculated. If the <i>propagate</i> parameter is <b>true</b> then complite branch
     starting from this node will be disabled.
     \param enabled if the value is <b>true</b> then the parameter will be enabled, otherwise
     it will be disabled
     \param propagate <b>false</b> will disable only this region, <b>true</b> will disable the whole branch
     */
    void stateChanged(bool enabled, bool propagate);

    /*! This function dumps the region to the given output stream
     */
    void print(std::ostream & out) const;

  private:
    Region(const std::string & name, Input & in, Output & out,
        boost::shared_ptr<Region> parent,
        const dqm_core::RegionConfig & config);
    ~Region();

    void handleResult(const std::string & name, const Result & result);

    boost::shared_ptr<Parameter> getParameter(const std::string & name,
        const dqm_core::ParameterConfig & config);

    boost::shared_ptr<Region> self();

  private:
    typedef std::map<std::string, boost::weak_ptr<Region> > RootRegions;

    static boost::recursive_mutex s_root_guard_;
    static RootRegions s_root_regions_;

  private:
    Input & input_;
    std::unique_ptr<SummaryMaker> summary_maker_;
    ParametersMap children_;
    boost::recursive_mutex mutex_;
  };
}

#endif
