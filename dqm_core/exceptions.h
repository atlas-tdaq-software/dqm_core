#ifndef _DQM_CORE_EXCEPTIONS_H_
#define _DQM_CORE_EXCEPTIONS_H_

/*! \file exceptions.h Declares DQM exceptions.
 * \author Serguei Kolos
 * \version 1.0
 */

#include <ers/ers.h>

namespace dqm_core_core {
  inline void fatal(const ers::Issue & issue) {
    ers::fatal(issue);
    exit(13);
  }
}

/*! \namespace dqm_core_core
 *  This is a wrapping namespace for all DQM exceptions and classes.
 */

/*! \class dqm_core::Exception
 *  This is a base class for all DQM exceptions.
 */
ERS_DECLARE_ISSUE( dqm_core, Exception, ERS_EMPTY, ERS_EMPTY )

/*! \class dqm_core::AlreadyExist
 *  This issue is reported if one tries to add to the Data Quality
 *  tree a new Region or Parameter which already exists in this tree.
 */
ERS_DECLARE_ISSUE_BASE( dqm_core,
    AlreadyExist,
    dqm_core::Exception,
    "DQM object '" << name << "' already exists in the '" << parent << "' DQM region",
    ERS_EMPTY,
    ((std::string)name )
    ((std::string)parent )
)

/*! \class dqm_core::CyclicRef
 *  This issue is reported if one tries to add to the Data Quality tree
 *  a new Region or Parameter which will result in a cyclic references in this tree.
 */
ERS_DECLARE_ISSUE_BASE( dqm_core,
    CyclicRef,
    dqm_core::Exception,
    "Adding DQM object '" << name << "' will add cyclic reference to the DQ tree",
    ERS_EMPTY,
    ((std::string)name )
)

/*! \class dqm_core::CantLoadLibrary
 *  This issue is reported if an attempt to load a dynamic library failes.
 */
ERS_DECLARE_ISSUE_BASE( dqm_core,
    CantLoadLibrary,
    dqm_core::Exception,
    "The error '" << error << "' received while trying to load '" << name << "' dynamic library",
    ERS_EMPTY,
    ((std::string)name )
    ((std::string)error )
)

/*! \class dqm_core::AlgorithmNotFound
 *  This issue is reported if nonexisting algorithm is requested to be used.
 */
ERS_DECLARE_ISSUE_BASE( dqm_core,
    AlgorithmNotFound,
    dqm_core::Exception,
    "DQM algorithm '" << name << "' is not known for the DQM algorithm manager",
    ERS_EMPTY,
    ((std::string)name )
)

/*! \class dqm_core::StreamNotFound
 *  This issue is reported if nonexisting stream is requested to be used.
 */
ERS_DECLARE_ISSUE_BASE( dqm_core,
    FactoryNotFound,
    dqm_core::Exception,
    "DQM " << type << " factory '" << name << "' is not known for the DQM stream manager",
    ERS_EMPTY,
    ((std::string)type )
    ((std::string)name )
)

/*! \class dqm_core::StreamConstructionIssue
 *  This issue is reported if nonexisting stream is requested to be used.
 */
ERS_DECLARE_ISSUE_BASE( dqm_core,
    StreamConstructionIssue,
    dqm_core::Exception,
    "Constructor of the " << type << " stream '" << name << "' failed",
    ERS_EMPTY,
    ((std::string)type )
    ((std::string)name )
)

/*! \class dqm_core::StreamAlreadyRegistered
 *  This issue is reported if nonexisting algorithm is requested to be used.
 */
ERS_DECLARE_ISSUE_BASE( dqm_core,
    FactoryAlreadyRegistered,
    dqm_core::Exception,
    "Another DQM " << type << " factory has already been registered with '" << name << "' name",
    ERS_EMPTY,
    ((std::string)type )
    ((std::string)name )
)

/*! \class dqm_core::SummaryMakerNotFound
 *  This issue is reported if nonexisting summarymaker is requested to be used.
 */
ERS_DECLARE_ISSUE_BASE( dqm_core,
    SummaryMakerNotFound,
    dqm_core::Exception,
    "DQM summary maker '" << name << "' is not known for the DQM algorithm manager",
    ERS_EMPTY,
    ((std::string)name )
)

/*! \class dqm_core::AlgorithmLibNotFound
 *  This issue is reported if nonexisting shared library is requested to be used.
 */
ERS_DECLARE_ISSUE_BASE( dqm_core,
    AlgorithmLibNotFound,
    dqm_core::Exception,
    "DQM algorithm shared library '" << name << "' is not known for the DQM library manager",
    ERS_EMPTY,
    ((std::string)name )
)

/*! \class dqm_core::BadConfig
 *  This issue is reported if configuration infomation given to DQM Parameter or DQM Region is invalid.
 */
ERS_DECLARE_ISSUE_BASE( dqm_core,
    BadConfig,
    dqm_core::Exception,
    "Invalid configuration (" << reason << ") was provided for the '" << name << "' object",
    ERS_EMPTY,
    ((std::string)name )
    ((std::string)reason )
)

/*! \class dqm_core::BadRefHist
 *  This issue is reported if the reference histogram given DQM Parameter is invalid.
 */

ERS_DECLARE_ISSUE_BASE( dqm_core,
    BadRefHist,
    dqm_core::Exception,
    "Invalid reference histogram(" << reason << ") was provided for the '" << name << "' object",
    ERS_EMPTY,
    ((std::string)name )
    ((std::string)reason )
)

/*! \class dqm_core::HistStat
 *  This issue is reported if histogram does not fulfill minimum statistics requirement
 */
ERS_DECLARE_ISSUE_BASE( dqm_core,
    HistStat,
    dqm_core::Exception,
    "Histogram  " << name << " did not fulfill minimum statistics requirement to perform algorithm",
    ERS_EMPTY,
    ((std::string)name )
)

/*! \class dqm_core::CantOpenLib
 *  This issue is reported if the shared library cannot be opend with dlopen
 */
ERS_DECLARE_ISSUE_BASE( dqm_core,
    CantOpenLib,
    dqm_core::Exception,
    "Cannot load library: (" << error << ") ",
    ERS_EMPTY,
    ((std::string)error )
)

/*! \class dqm_core::FileNotReadable
 *  This issue is reported if a file cannot be found, cannot be opened, or cannot be read,
 *  for example due to corruption.
 */
ERS_DECLARE_ISSUE_BASE( dqm_core,
    FileNotReadable,
    dqm_core::Exception,
    "File not readable: " << name,
    ERS_EMPTY,
    ((std::string)name )
)

/*! \class dqm_core::FileNotWriteable
 *  This issue is reported if a file cannot be opened for writing.
 */
ERS_DECLARE_ISSUE_BASE( dqm_core,
    FileNotWriteable,
    dqm_core::Exception,
    "File not writeable: " << name,
    ERS_EMPTY,
    ((std::string)name )
)

/*! \class dqm_core::MonObjectNotFound
 *  This issue is reported if a monitoring object, e.g., a histogram, cannot be found
 *  at the requested location.
 */
ERS_DECLARE_ISSUE_BASE( dqm_core,
    MonObjectNotFound,
    dqm_core::Exception,
    "Object not found: " << name,
    ERS_EMPTY,
    ((std::string)name )
)

/*! \class dqm_core::RegexMatchFailed
 *  This issue is reported if a histogram name does not match the given regular expression.
 */
ERS_DECLARE_ISSUE_BASE( dqm_core,
    RegexMatchFailed,
    dqm_core::Exception,
    "The '" << name << "' input object name does not match the '" << regex << "' regular expression",
    ERS_EMPTY,
    ((std::string)name )
    ((std::string)regex )
)
#endif // _DQM_CORE_EXCEPTIONS_H_
