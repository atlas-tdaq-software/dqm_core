#ifndef DQM_CORE_ALGORITHM_H
#define DQM_CORE_ALGORITHM_H

#include <iostream>

#include <dqm_core/AlgorithmConfig.h>
#include <dqm_core/Result.h>

#ifndef __MAKECINT__
#include <dqm_core/exceptions.h>
#endif

class TObject;
namespace dqm_core {
  class Algorithm {
  public:
    virtual ~Algorithm() {
      ;
    }

    virtual dqm_core::Algorithm * clone() = 0;

    virtual dqm_core::Result * execute(const std::string & name,
        const TObject & object, const dqm_core::AlgorithmConfig &) = 0;

    virtual void printDescriptionTo(std::ostream &) {
      ;
    }

    virtual void printDescription() {
      printDescriptionTo(std::cout);
    }
  };
}

#endif // DQM_CORE_ALGORITHM_H 
