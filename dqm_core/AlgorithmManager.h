#ifndef _DQM_CORE_ALGORITHM_MANAGER_H_
#define _DQM_CORE_ALGORITHM_MANAGER_H_

/*! \file AlgorithmManager.h Declares the dqm_core::AlgorithmManager class.
 * \author 
 * \version 1.0
 */
#include <map>

#include <boost/thread/mutex.hpp>

#include <dqm_core/Algorithm.h>
#include <dqm_core/SummaryMaker.h>
#include <dqm_core/exceptions.h>

/*! \namespace dqm_core
 *  This is a wrapping namespace for all public classes of the
 *  Data Quality Monitoring framework.
 */
namespace dqm_core {
  /*! \brief This class implements a singleton which manages DQM algorithms.
   \ingroup public
   \sa	dqm_core::Algorithm, dqm_core::SummaryMaker
   */
  class AlgorithmManager {
  public:

    static AlgorithmManager & instance();

    void registerAlgorithm(const std::string & name, Algorithm * algorithm);

    void registerAlgorithm(const std::string & name, SummaryMaker * summaker);

    void registerSummaryMaker(const std::string & name,
        SummaryMaker * summaker);

    Algorithm * getAlgorithm(const std::string & name) const;

    SummaryMaker * getSummaryMaker(const std::string & name) const;

    const std::map<std::string, Algorithm*> & getAlgorithmMap() const {
      return algorithms_;
    }

    const std::map<std::string, SummaryMaker*> & getSummaryMakerMap() const {
      return summaryMakers_;
    }

  private:
    AlgorithmManager();

  private:
    std::map<std::string, Algorithm*> algorithms_;
    std::map<std::string, SummaryMaker*> summaryMakers_;

    static boost::mutex mutex_;
    static AlgorithmManager * instance_;
  };
}

#endif
