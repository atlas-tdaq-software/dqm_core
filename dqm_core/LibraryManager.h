#ifndef DQM_CORE_LIBRARY_MANAGER_H
#define DQM_CORE_LIBRARY_MANAGER_H

/*
 *  LibraryManager.h
 *  dqm_core
 *
 *  Copyright 2005 CERN. All rights reserved.
 *
 */

/** \file LibraryManager.h This file defines LibraryManager class.
 * \brief dqm_core header file
 */

#include <string>
#include <map>

#include <boost/shared_ptr.hpp>
#include <boost/thread.hpp>
#include <boost/noncopyable.hpp>

#include <ers/internal/SingletonCreator.h>
#include <dqm_core/exceptions.h>

namespace dqm_core {
  /*! \brief This class implements a singleton which manages DQM libraries which implement
   custom algorithms or streams.
   \ingroup public
   \sa	dqm_core::Algorithm, dqm_core::SummaryMaker, dqm_core::InputFactory, dqm_core::OutputFactory
   */
  class LibraryManager: boost::noncopyable {
    template<class > friend class ers::SingletonCreator;

    class SharedLibrary: boost::noncopyable {
    public:
      /**	 Loads the shared library and stores a library handle.
       @param name Name of the library to be loaded.
       */
      SharedLibrary(const std::string & name);

      /** Unload the dynamic library.  */
      ~SharedLibrary();

    private:
      void * m_handle;
    };

  public:
    /** Destructor unloads the plugins.
     */
    ~LibraryManager();

    static LibraryManager & instance(); /**< \brief return the singleton */

    void loadLibrary(const std::string & name);

  private:
    typedef std::map<std::string, boost::shared_ptr<SharedLibrary> > LibMap;

    boost::mutex m_mutex;
    LibMap m_libraries;

  private:
    /** Constructor loads the plugins.
     */
    LibraryManager();

  };
}

#endif
