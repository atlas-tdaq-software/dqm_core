#ifndef _DQM_CORE_REGION_PROXY_H_
#define _DQM_CORE_REGION_PROXY_H_

/*! \file RegionProxy.h Declares the dqm_core::RegionProxy class.
 * \author Serguei Kolos
 * \version 1.0
 */

#include <dqm_core/OutputListener.h>
#include <dqm_core/Node.h>

/*! \namespace dqm_core
 *  This is a wrapping namespace for all public classes of the Data Quality Monitoring framework.
 */
namespace dqm_core {
  class Region;
  class Output;

  class RegionProxy: public Node, public OutputListener {
  public:
    static boost::shared_ptr<RegionProxy> create(const std::string & name,
        Output & out, boost::shared_ptr<Region> parent,
        const dqm_core::RegionConfig & config);

  private:
    RegionProxy(const std::string & name, Output & out,
        boost::shared_ptr<Region> parent,
        const dqm_core::RegionConfig & config);

    ~RegionProxy();

    /*! Always returns false.
     */
    bool isLeaf() const {
      return false;
    }

    void handleResult(const std::string & name, const Result & result);
  };
}

#endif
