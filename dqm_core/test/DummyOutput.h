#ifndef _DQM_CORE_DUMMY_OUTPUT_H_
#define _DQM_CORE_DUMMY_OUTPUT_H_

/*! \file DummyOutput.h Declares the dqm_core::DummyOutput class.
 * \author Serguei Kolos
 * \version 1.0
 */

#include <iostream>

#include <dqm_core/Output.h>

/*! \namespace dqm_core
 *  This is a wrapping namespace for all public classes of the Data Quality Monitoring framework.
 */
namespace dqm_core {
  namespace test {
    /*! \brief This class provides a simple dummy implementation of the abstract
     * output interface for publishing infomation which is produced by the Data Quality
     * assessment algorithms.
     * \ingroup public
     * \sa dqm_core::Output
     */
    class DummyOutput: public Output {
    public:
      DummyOutput(const std::vector<std::string> & params);

      ~DummyOutput();

      /*! This function is used to put the given DQ result to the appropriate place.
       */
      virtual void publishResult(const std::string & name, const Result & result);

      /*! This function is called to register a listener object with a particular object in this
       output stream. The OutputListener::handleResult function will be called by this Output
       stream whenever a dqm_core::Result object for the dqm_core::Parameter with the given
       name becomes available in this stream.
       \param name parameters' name
       \param listener listener associated with the given parameter name
       */
      virtual void addListener(const std::string & name, OutputListener * listener);

      /*! This function activates this Output stream until the Output::deactivate function is called.
       Activating means that the stream will accept the DQ result and pass them to the appropriate
       place and will aslo start notifying the listeners which have been registered with it.
       \sa deactivate
       */
      virtual void activate();

      /*! This function blocks this Output stream until the Output::activate function is called. Blocking
       means that when this function returns it is guaranteed that listeners which have been
       registered with this stream will not be notified anymore and any calls to the Output::publishResult
       will have no effect. The function has no effect if the stream is already in the inactive state.
       \sa activate
       */
      virtual void deactivate();

    private:
      void callback();

      std::ostream * output_;
      bool active_;
      std::map<std::string, OutputListener*> listeners_;
    };
  }
}

#endif
