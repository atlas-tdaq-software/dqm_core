/*! \file DummySummary.h file declares the dqm_core::summary::DummySummary class.
 * \author Haleh Hadavand
 */

#ifndef DQM_CORE_DUMMY_SUMMARY_H
#define DQM_CORE_DUMMY_SUMMARY_H

#include <dqm_core/SummaryMaker.h>

namespace dqm_core {
  namespace test {
    struct DummySummary: public dqm_core::SummaryMaker {
      SummaryMaker * clone();

      dqm_core::Result * execute(const std::string &, const dqm_core::Result &,
          const dqm_core::ParametersMap &);
    };
  }
}

#endif
