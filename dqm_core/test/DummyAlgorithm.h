/*! \file DummyAlgorithm.h file declares the dqm_core::algorithm::DummyAlgorithm  class.
 * \author Haleh Hadavand
 */

#ifndef DQM_CORE_DUMMYALGORITHM_H
#define DQM_CORE_DUMMYALGORITHM_H

#include <dqm_core/Algorithm.h>

namespace dqm_core {
  namespace test {
    struct DummyAlgorithm: public dqm_core::Algorithm {
      dqm_core::Algorithm * clone();

      dqm_core::Result * execute(const std::string &, const TObject &,
          const dqm_core::AlgorithmConfig &);

      void printDescriptionTo(std::ostream & out);
    };
  }
}

#endif
