#ifndef _DQM_CORE_DUMMY_ALGORITHM_CONFIG_H_
#define _DQM_CORE_DUMMY_ALGORITHM_CONFIG_H_

/*! \file DummyAlgorithmConfig.h Declares the dqm_core::DummyAlgorithmConfig class.
 * \author Serguei Kolos
 * \version 1.0
 */

#include <TH1.h>
#include <dqm_core/AlgorithmConfig.h>
//#include <dqm_core/exceptions.h>
/*! \namespace dqm_core
 *  This is a wrapping namespace for all public classes of the Data Quality Monitoring framework.
 */
namespace dqm_core {
  namespace test {
    /*! \brief This class provides a simple dummy implementation of the DQMF
     * abstract AlgorithmConfig interface which can be used for testing the core of DQMF.
     * \ingroup public
     */
    struct DummyAlgorithmConfig: public AlgorithmConfig {
      DummyAlgorithmConfig() :
          ref_(0) {
        ;
      }

      DummyAlgorithmConfig(TObject * ref) :
          ref_(ref) {
        ;
      }

      void setReference(TObject * ref);

      void addGenericParameter(const std::string & name,
          const std::string & value);

      void addParameter(const std::string & name, double value);

      void addGreenThreshold(const std::string & name, double value);

      void addRedThreshold(const std::string & name, double value);

      virtual TObject * getReference() const;

    private:
      TObject * ref_;
    };
  }
}

#endif
