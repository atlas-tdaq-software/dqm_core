#ifndef _DQM_CORE_DUMMY_INPUT_H_
#define _DQM_CORE_DUMMY_INPUT_H_

/*! \file Input.h Declares the dqm_core::Input class.
 * \author Serguei Kolos
 * \version 1.0
 */

#include <thread>
#include <boost/noncopyable.hpp>
#include <map>

#include <dqm_core/Input.h>

/*! \namespace dqm_core
 *  This is a wrapping namespace for all public classes of the Data Quality Monitoring framework.
 */
namespace dqm_core {
  namespace test {
    /*! \brief This class provides a simple dummy implementation of the DQMF abstract input interface
     which can be used for testing the core of DQMF.
     \ingroup public
     \sa	dqm_core::InputListener
     */
    struct DummyInput: public Input, boost::noncopyable {
      /*! Constructor.
       */
      DummyInput(const std::vector<std::string> & params);

      /*! Destructor.
       */
      ~DummyInput();

      /*! This function is called to register a listener object with a particular object in the
       input stream. The InputListener::handleInput function will be called by this Input
       stream whenever an object with the given name becomes available in the stream.
       \param name objects's name
       \param listener listener associated with the given object name
       */
      virtual void addListener(const std::string & name,
          InputListener * listener);

      /*! This function is called to register a listener object with a set of objects in the
       input stream. The InputListener::handleInput function will be called by this Input
       stream whenever a single object from this set becomes available in the stream.
       \param regex Posix style regular expression for the objects' names
       \param listener listener associated with the given regular expression
       */
      virtual void addListener(const boost::regex & regex,
          InputListener * listener);

      /*! This function is called to register a listener object with a set of objects in the
       input stream. The InputListener::handleInput function will be called by this Input
       stream whenever a single object from this set becomes available in the stream.
       \param names vecotor of objects' names
       \param listener listener associated with the given objects' set
       */
      virtual void addListener(const std::vector<std::string> & names,
          InputListener * listener);

      /*! This function blocks this Input stream until the Input::activate function is called. Blocking
       means that when this function returns it is guaranteed that listeners which have been
       registered with this stream are notified anymore. The function has no effect if the
       stream has been already in the blocked state.
       \sa activate
       */
      virtual void deactivate();

      /*! This function activates this Input stream until the Input::block function is called. Activating
       means that the stream will start notifying the listeners which have been registered with it.
       \sa deactivate
       */
      virtual void activate();

      /*! This function breaks the wait function if it was active.
       \sa block
       */
      virtual void stop();

      /*! This function blocks until there is some data still in this Input stream.
       \sa block
       */
      virtual void wait();

    private:
      void simulator();

      std::map<std::string, InputListener*> listeners_;
      InputListener * regex_listener_;
      boost::regex regex_;
      unsigned int period_;
      bool blocked_;
      std::thread * thread_;
    };
  }
}

#endif
