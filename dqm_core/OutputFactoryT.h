#ifndef _DQM_CORE_OUTPUT_FACTORY_T_H_
#define _DQM_CORE_OUTPUT_FACTORY_T_H_

/*! \file OutputFactory.h Declares the dqm_core::OutputFactory interface.
 * \author Serguei Kolos
 * \version 1.0
 */

#include <dqm_core/OutputFactory.h>
#include <dqm_core/IOManager.h>

/*! \namespace dqm_core
 *  This is a wrapping namespace for all public classes of the Data Quality Monitoring framework.
 */
namespace dqm_core {
  class Output;

  /*! \brief This class is a simple helper for creating a user specific Output Factory object.
   \ingroup public
   */
  template<class T>
  struct OutputFactoryT: public OutputFactory {
    const std::string m_name;

    OutputFactoryT(const std::string & name) :
        m_name(name) {
      try {
        dqm_core::IOManager::instance().registerOutputFactory(m_name, this);
      }
      catch (dqm_core::Exception & ex) {
        ers::error(ex);
      }
    }

    /*! This function is called to create a new Output object.
     \param params parameters for the new output stream
     */
    Output * createOutput(const std::vector<std::string> & params) {
      try {
        return new T(params);
      }
      catch (ers::Issue & ex) {
        throw dqm_core::StreamConstructionIssue(ERS_HERE, "output", m_name, ex);
      }
      catch (std::exception & ex) {
        throw dqm_core::StreamConstructionIssue(ERS_HERE, "output", m_name, ex);
      }
    }

  };
}

#endif
