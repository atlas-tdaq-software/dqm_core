#ifndef _DQM_CORE_INPUT_FACTORY_T_H_
#define _DQM_CORE_INPUT_FACTORY_T_H_

/*! \file InputFactory.h Declares the dqm_core::InputFactory interface.
 * \author Serguei Kolos
 * \version 1.0
 */

#include <dqm_core/InputFactory.h>
#include <dqm_core/IOManager.h>
#include <dqm_core/exceptions.h>

/*! \namespace dqm_core
 *  This is a wrapping namespace for all public classes of
 *  the Data Quality Monitoring framework.
 */
namespace dqm_core {
  class Input;

  /*! \brief This class is a simple helper for creating
   * a user specific Input Factory object.
   \ingroup public
   */
  template<class T>
  struct InputFactoryT: public InputFactory {
    const std::string m_name;

    InputFactoryT(const std::string & name) :
        m_name(name) {
      try {
        dqm_core::IOManager::instance().registerInputFactory(m_name, this);
      }
      catch (dqm_core::Exception & ex) {
        ers::error(ex);
      }
    }

    /*! This function is called to create a new Input object.
     \param params parameters for the new output stream
     */
    Input * createInput(const std::vector<std::string> & params) {
      try {
        return new T(params);
      }
      catch (ers::Issue & ex) {
        throw dqm_core::StreamConstructionIssue(ERS_HERE, "input", m_name, ex);
      }
      catch (std::exception & ex) {
        throw dqm_core::StreamConstructionIssue(ERS_HERE, "input", m_name, ex);
      }
    }
  };
}

#endif
