#pragma link C++ namespace dqm_core;
#pragma link off all globals;
#pragma link off all classes;
#pragma link off all functions;

#pragma link C++ class dqm_core::Result-!;
#pragma link C++ class dqm_core::AlgorithmConfig-!;
#pragma link C++ class dqm_core::Algorithm-!;
