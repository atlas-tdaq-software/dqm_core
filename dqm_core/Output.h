#ifndef _DQM_CORE_OUTPUT_H_
#define _DQM_CORE_OUTPUT_H_

/*! \file Output.h Declares the dqm_core::Output class.
 * \author Serguei Kolos
 * \version 1.0
 */

#include <dqm_core/exceptions.h>
#include <dqm_core/Result.h>

/*! \namespace dqm_core
 *  This is a wrapping namespace for all public classes of the Data Quality Monitoring framework.
 */
namespace dqm_core {
  class OutputListener;
  class Parameter;

  /*! \brief This class defines abstract output interface for publishing information
   which is produced by the Data Quality assessment algorithms.
   \ingroup public
   */
  struct Output {
    /*! Virtual destructor is defined to make sure that destructor of the sub-class will be
     called whenever the delete operator is applied to the dqm_core::Output pointer.
     */
    virtual ~Output() {
      ;
    }

    /*! This function is called to register a listener object with a particular object in this
     output stream. The OutputListener::handleResult function will be called by this Output
     stream whenever a dqm_core::Result object for the dqm_core::Parameter with the given
     name becomes available in this stream.
     \param name parameters' name
     \param listener listener associated with the given parameter
     \sa virtual void addListener( const Parameter & parameter, OutputListener * listener )
     */
    virtual void addListener(const std::string & name,
        OutputListener * listener) = 0;

    /*! This function is used to put the given DQ result to an appropriate place.
     */
    virtual void publishResult(const std::string & name,
        const Result & result) = 0;

    /*! This function activates this Output stream until the Output::deactivate function is called.
     Activating means that the stream will accept the DQ result and pass them to the appropriate
     place and will also start notifying the listeners which have been registered with it.
     \sa deactivate
     */
    virtual void activate() = 0;

    /*! This function blocks this Output stream until the Output::activate function is called. Blocking
     means that when this function returns it is guaranteed that listeners which have been
     registered with this stream will not be notified anymore and any calls to the Output::publishResult
     will have no effect. The function has no effect if the stream is already in the inactive state.
     \sa activate
     */
    virtual void deactivate() = 0;
  };
}

#endif
