#ifndef _DQM_CORE_ALGORITHM_CONFIG_H_
#define _DQM_CORE_ALGORITHM_CONFIG_H_

/*! \file AlgorithmConfig.h Declares the dqm_core::AlgorithmConfig class.
 * \author Serguei Kolos
 * \version 1.0
 */

#include <iostream>
#include <map>
#include <string>

class TObject;

/*! \namespace dqm_core
 *  This is a wrapping namespace for all public classes of the Data Quality Monitoring framework.
 */
namespace dqm_core {
  /*! \brief This class provides the configuration information for a DQM Algorithm.
   \ingroup public
   \sa	dqm_core::Algorithm
   */
  class AlgorithmConfig {
  public:
    virtual ~AlgorithmConfig() {
      ;
    }

    virtual TObject * getReference() const {
      return 0;
    }
    ;

    virtual const std::map<std::string, std::string> & getGenericParameters() const {
      return m_generic_parameters;
    }

    virtual const std::map<std::string, double> & getParameters() const {
      return m_parameters;
    }

    virtual const std::map<std::string, double> & getGreenThresholds() const {
      return m_green_thresholds;
    }

    virtual const std::map<std::string, double> & getRedThresholds() const {
      return m_red_thresholds;
    }

    /*! This function can be used to disable algorithm execution under
     * some condition. This function is called before each attempt of
     * executing DQ algorithm. If it returns false, the framework will
     * not run the corresponding algorithm.
     */
    virtual bool algorithmExecutionPermitted() const {
      return true;
    }

    virtual void lock() {
      ;
    }

    virtual void unlock() {
      ;
    }

    /*! This function dumps the object to the given output stream
     */
    virtual void print(std::ostream & out) const;

  protected:
    mutable std::map<std::string, std::string> m_generic_parameters;
    mutable std::map<std::string, double> m_parameters;
    mutable std::map<std::string, double> m_green_thresholds;
    mutable std::map<std::string, double> m_red_thresholds;
  };
}

#endif
