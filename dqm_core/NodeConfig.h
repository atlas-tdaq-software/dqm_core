#ifndef _DQM_CORE_NODE_CONFIG_H_
#define _DQM_CORE_NODE_CONFIG_H_

/*! \file NodeConfig.h Declares the dqm_core::NodeConfig class.
 * \author Serguei Kolos
 * \version 1.0
 */

#include <string>
#include <vector>

#include <boost/shared_ptr.hpp>

#include <dqm_core/AlgorithmConfig.h>

/*! \namespace dqm_core
 *  This is a wrapping namespace for all public classes of the Data Quality Monitoring framework.
 */
namespace dqm_core {
  /*! \brief This class provides the configuration information for a DQM Parameter.
   \ingroup public
   \sa	dqm_core::RegionConfig
   */
  struct NodeConfig {
    NodeConfig(const std::string & algorithm_name, float weight, bool enabled =
        true) :
        algorithm_name_(algorithm_name), weight_(weight), enabled_(enabled) {
      ;
    }

    const std::string algorithm_name_;
    const float weight_;
    bool enabled_;
  };
}

#endif
