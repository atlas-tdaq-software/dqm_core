#ifndef _DQM_CORE_SUMMARY_MAKER_H_
#define _DQM_CORE_SUMMARY_MAKER_H_

/*! \file SummaryMaker.h Declares the dqm_core::SummaryMaker class.
 * \author Serguei Kolos
 * \version 1.0
 */

#include <map>
#include <string>

#include <boost/shared_ptr.hpp>

#include <dqm_core/exceptions.h>

/*! \namespace dqm_core
 *  This is a wrapping namespace for all public classes of the Data Quality Monitoring framework.
 */
namespace dqm_core {
  class Node;
  class Result;

  typedef std::map<std::string, boost::shared_ptr<Node>> ParametersMap;

  struct SummaryMaker {

    virtual ~SummaryMaker() {
      ;
    }

    virtual dqm_core::Result * execute(const std::string & name,
        const Result & last_result,
        const dqm_core::ParametersMap & children) = 0;

    virtual dqm_core::SummaryMaker * clone() = 0;
  };
}

#endif
