#ifndef _DQM_STATE_CHANGE_LISTENER_H_
#define _DQM_STATE_CHANGE_LISTENER_H_

/*! \file StateChangeListener.h Declares the dqm_core::StateChangeListener class.
 * \author Serguei Kolos
 * \version 1.0
 */

/*! \namespace dqm_core
 *  This is a wrapping namespace for all public classes of the Data Quality Monitoring framework.
 */
namespace dqm_core {
  /*! \brief This class defines abstract interface for the notification of the state
   changes for Parameter or Region.
   functions.
   \ingroup public
   \sa	dqm_core::InputListener
   */
  struct StateChangeListener {
    /*! Virtual destructor is defined to make sure that dustructor of the sub-class will be
     called whenever the delete operator is applyed to the dqm_core::StateChangeListener pointer.
     */
    virtual ~StateChangeListener() {
      ;
    }

    /*! This function is called whenever an object for which this listener has been
     enabled or disabled.
     \param enabled true if this object has to be enabled, false if it has to be disabled
     \param propagate true if the new state has to be propagated to the children
     */
    virtual void stateChanged(bool enabled, bool propagate) = 0;

    /*! This function instructs the listener to reset itself to the initial state.
     \param notify_parents 	if the value is <b>true</b> then the listener shall notify its parents of the state change
     \param propagate 		if the value is <b>true</b> then the listener shall reset its children
     */
    virtual void reset(bool notify_parents, bool propagate) = 0;
  };
}

#endif
