#ifndef _DQM_CORE_UTILS_H_
#define _DQM_CORE_UTILS_H_

/*! \file Utils.h Declares the helper algorithms for dqm_core.
 * \author Serguei Kolos
 * \version 1.0
 */

#include <dqm_core/Node.h>

/*! \namespace dqm_core
 *  This is a wrapping namespace for all public classes of the Data Quality Monitoring framework.
 */
namespace dqm_core {
  template<class Iterator>
  boost::shared_ptr<Node> plain_accessor(Iterator it) {
    return *it;
  }

  template<class Iterator>
  boost::shared_ptr<Node> map_accessor(Iterator it) {
    return it->second;
  }

  //! Finds the node with the given name.
  /*!
   This function searches for the node associated with the given name in all branches
   of the nodes of the given container.

   \param name of the node which has to be found
   \param first first node whose branches have to be searched
   \param last last node whose branches have to be searched
   \param propagate parameter for the enable call
   \param value_extractor defines how to get Node objects from the given container
   There are two predefined extractors:
   - plain_accessor can be used to get objects from plain std containers like, list, vector, etc.
   - map_accessor can be used to get objects from std::map.
   \return pointer to the found node, otherwise zero
   */
  template<class Iterator>
  boost::shared_ptr<Node> find(const std::string & name, Iterator first,
      Iterator last,
      boost::shared_ptr<Node> (value_accessor)(
          Iterator it) = plain_accessor<Iterator>) {
    boost::shared_ptr<Node> r;
    for (Iterator it = first; it != last; ++it) {
      boost::shared_ptr<Node> parent = value_accessor(it);
      if (parent.get()) {
        r = parent->find(name);
        if (r.get() != 0)
          break;
      }
    }
    return r;
  }

  //! Finds node with the given name.
  /*! This function searches for the node associated with the given name
   in the scope of the current application.
   \param name parameter name
   \return	    empty shared pointer if there is no region/paramater associated with the given name,
   otherwise a pointer to the target object
   */
  boost::shared_ptr<dqm_core::Node>
  find(const std::string & name);

  //! Enables the node with the given name.
  /*!
   This function searches for the node associated with the given name in all branches
   of the given root node and enables the target node if finds it.
   \param name of the node which has to be enabled
   \param root all branches belonging to this node will be searched
   \param propagate parameter for the enable call
   \return <b>true</b> is the operation was successful, <b>false</b> otherwise
   \sa void dqm_core::Parameter::stateChanged( bool enabled, bool propagate )
   \sa void dqm_core::Region::stateChanged( bool enabled, bool propagate )
   */
  bool enable(const std::string & name, boost::shared_ptr<Node> root,
      bool propagate);

  //! Disables the node with the given name.
  /*!
   This function searches for the node associated with the given name in all branches
   of the given root node and disables the target node if finds it.
   \param name of the node which has to be disabled
   \param root all branches belonging to this node will be searched
   \param propagate parameter for the disable call
   \return <b>true</b> is the operation was successful, <b>false</b> otherwise
   \sa void dqm_core::Parameter::stateChanged( bool enabled, bool propagate )
   \sa void dqm_core::Region::stateChanged( bool enabled, bool propagate )
   */
  bool disable(const std::string & name, boost::shared_ptr<Node> root,
      bool propagate);

  //! Enables the node with the given name.
  /*!
   This function searches for the node associated with the given name in all branches
   of the nodes of the given container and enables the target node if finds it. This
   function can enable one node at maximum. If different nodes in the container contain
   objects associated with the <b>name</b> name then only the first one which
   is found by the algorithm will be enabled. All other are ignored.

   \param name of the node which has to be enabled
   \param first first node whose branches have to be searched
   \param last last node whose branches have to be searched
   \param propagate parameter for the enable call
   \param value_extractor defines how to get Node objects from the given container
   There are two predefined extractors:
   - plain_accessor can be used to get objects from plain std containers like, list, vector, etc.
   - map_accessor can be used to get objects from std::map.
   \return <b>true</b> is the operation was successful, <b>false</b> otherwise
   \sa void dqm_core::Parameter::stateChanged( bool enabled, bool propagate )
   \sa void dqm_core::Region::stateChanged( bool enabled, bool propagate )
   */
  template<class Iterator>
  bool enable(const std::string & name, Iterator first, Iterator last,
      bool propagate,
      boost::shared_ptr<Node> (value_accessor)(
          Iterator it) = plain_accessor<Iterator>) {
    boost::shared_ptr<Node> p = find(name, first, last, value_accessor);
    if (p.get()) {
      p->stateChanged(true, propagate);
      return true;
    }
    return false;
  }

  //! Disables the node with the given name.
  /*!
   This function searches for the node associated with the given name in all branches
   of the nodes of the given container and disables the target node if finds it. This
   function can disable one node at maximum. If different nodes in the container contain
   objects associated with the <b>name</b> name then only the first one which
   is found by the algorithm will be disabled. All other are ignored.

   \param name of the node which has to be disabled
   \param first first node whose branches have to be searched
   \param last last node whose branches have to be searched
   \param propagate parameter for the disable call
   \param value_extractor defines how to get Node objects from the given container
   There are two predefined extractors:
   - plain_accessor can be used to get objects from plain std containers like, list, vector, etc.
   - map_accessor can be used to get objects from std::map.
   \return <b>true</b> is the operation was successful, <b>false</b> otherwise
   \sa void dqm_core::Parameter::stateChanged( bool enabled, bool propagate )
   \sa void dqm_core::Region::stateChanged( bool enabled, bool propagate )
   */
  template<class Iterator>
  bool disable(const std::string & name, Iterator first, Iterator last,
      bool propagate,
      boost::shared_ptr<Node> (value_accessor)(
          Iterator it) = plain_accessor<Iterator>) {
    boost::shared_ptr<Node> p = find(name, first, last, value_accessor);
    if (p.get()) {
      p->stateChanged(false, propagate);
      return true;
    }
    return false;
  }
}

#endif
