#ifndef _DQM_CORE_INPUT_H_
#define _DQM_CORE_INPUT_H_

/*! \file Input.h Declares the dqm_core::Input class.
 * \author Serguei Kolos
 * \version 1.0
 */

#include <string>
#include <vector>

#include <boost/regex.hpp>

#include <dqm_core/exceptions.h>

/*! \namespace dqm_core
 *  This is a wrapping namespace for all public classes of the Data Quality Monitoring framework.
 */
namespace dqm_core {
  struct InputListener;

  /*! \brief This class defines abstract input interface for reading information for the
   Data Quality assessment.
   \ingroup public
   \sa	dqm_core::InputListener
   */
  struct Input
  {
    /*! Virtual destructor is defined to make sure that destructor of the sub-class will be
     called whenever the delete operator is applied to the dqm_core::Input pointer.
     */
    virtual ~Input()
    {
      ;
    }

    /*! This function is called to register a listener object with a particular object in the
     input stream. The InputListener::handleInput function will be called by this Input
     stream whenever an object with the given name becomes available in the stream.
     \param name objects' name
     \param listener listener associated with the given object name
     */
    virtual void addListener(const std::string & name,
        InputListener * listener) = 0;

    /*! This function is called to register a listener object with a set of objects in the
     input stream. The InputListener::handleInput function will be called by this Input
     stream whenever a single object from this set becomes available in the stream.
     \param regex Posix style regular expression for the objects' names
     \param listener listener associated with the given regular expression
     */
    virtual void addListener(const boost::regex & regex,
        InputListener * listener) = 0;

    /*! This function is called to register a listener object with a set of objects in the
     input stream. The InputListener::handleInput function will be called by this Input
     stream whenever a single object from this set becomes available in the stream.
     \param names vecotor of objects' names
     \param listener listener associated with the given objects' set
     */
    virtual void addListener(const std::vector<std::string> & names,
        InputListener * listener) = 0;

    /*! This function activates this Input stream until the Input::deactivate
     * function is called. Activating means that the stream will start notifying
     * the listeners which have been registered with it.
     * \sa deactivate
     */
    virtual void activate() = 0;

    /*! This function blocks this Input stream until the Input::activate function is called.
     * Blocking means that when this function returns it is guaranteed that listeners which
     * have been registered with this stream will not be notified anymore. The function has
     * no effect if the stream is already in the inactive state.
     * \sa activate
     */
    virtual void deactivate() = 0;

    /*! If the wait function is active then calling this one causes it to return. Otherwise has no effect.
     \sa wait
     */
    virtual void stop() = 0;

    /*! This function blocks until there is some data in this Input stream or the stop function is called.
     \sa stop
     */
    virtual void wait() = 0;

  };
}

#endif
