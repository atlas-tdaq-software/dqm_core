#ifndef _DQM_CORE_OUTPUT_FACTORY_H_
#define _DQM_CORE_OUTPUT_FACTORY_H_

/*! \file OutputFactory.h Declares the dqm_core::OutputFactory interface.
 * \author Serguei Kolos
 * \version 1.0
 */

#include <dqm_core/exceptions.h>

/*! \namespace dqm_core
 *  This is a wrapping namespace for all public classes of the Data Quality Monitoring framework.
 */
namespace dqm_core {
  class Output;

  /*! \brief This class defines abstract interface for creating dqm_core::Output objects.
   \ingroup public
   */
  struct OutputFactory {
    /*! Virtual destructor is defined to make sure that destructor of the sub-class will be
     called whenever the delete operator is applied to the dqm_core::OutputFactory pointer.
     */
    virtual ~OutputFactory() {
      ;
    }

    /*! This function is called to create a new Output object.
     \param params parameters' for the new output stream
     */
    virtual Output * createOutput(const std::vector<std::string> & params) = 0;
  };
}

#endif
