#ifndef _DQM_CORE_RESULT_H_
#define _DQM_CORE_RESULT_H_

/*! \file Result.h Declares the dqm_core::Result class.
 * \author Serguei Kolos
 * \version 1.0
 */

#include <map>
#include <memory>
#include <string>

#ifndef __MAKECINT__
#include <boost/date_time/posix_time/posix_time.hpp>
#endif

#include <TObject.h>

/*! \namespace dqm_core
 *  This is a wrapping namespace for all public classes of the Data Quality Monitoring framework.
 */
namespace dqm_core {
  struct Result {
    enum Status {
      Disabled = -1, Undefined, Red, Yellow, Green
    };

    Result() :
        status_(Undefined), lumi_block_(0), universal_time_(
            boost::posix_time::microsec_clock::universal_time()) {
      ;
    }

#ifndef __MAKECINT__
    Result(Status status, TObject * object = 0,
        const boost::posix_time::ptime & time =
            boost::posix_time::microsec_clock::universal_time()) :
        status_(status), object_(object), lumi_block_(0), universal_time_(time) {
      ;
    }

    boost::posix_time::ptime getLocalTime() const;
#endif

    TObject * getObject() const;

    std::string getTimeString() const;

    Result * clone() const;

    Status status_;
    std::map<std::string, double> tags_;
#ifndef __MAKECINT__
    boost::shared_ptr<TObject> object_;
    int lumi_block_;
    const boost::posix_time::ptime universal_time_;
#endif
  private:
    Result(const Result&);
    Result& operator=(const Result&);
  };
}

std::ostream & operator<<(std::ostream & out, dqm_core::Result::Status status);
std::ostream & operator<<(std::ostream & out, const dqm_core::Result & status);

#endif
