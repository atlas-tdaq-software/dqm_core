#ifndef _DQM_CORE_INPUT_MULTIPLEXOR_H_
#define _DQM_CORE_INPUT_MULTIPLEXOR_H_

/*! \file InputMultiplexor.h Declares the dqm_core::InputMultiplexor class.
 * \author Serguei Kolos
 * \version 1.0
 */

#include <boost/shared_ptr.hpp>

#include <dqm_core/Input.h>

/*! \namespace dqm_core
 *  This is a wrapping namespace for all public classes of
 *  the Data Quality Monitoring framework.
 */
namespace dqm_core {
  class InputMultiplexor: public dqm_core::Input {

  public:

    InputMultiplexor(dqm_core::Input * first = 0, dqm_core::Input * second = 0,
        dqm_core::Input * third = 0);

    /*! Add nested stream. The multiplexor object will become an owner
     *  of this stream and will delete it in its destructor.
     */
    void addInput(dqm_core::Input * input);

  private:

    /*! Calls this function for all nested streams.
     */
    void addListener(const std::string & name,
        dqm_core::InputListener * listener);

    /*! Calls this function for all nested streams.
     */
    void addListener(const boost::regex & regex, InputListener * listener);

    /*! Calls this function for all nested streams.
     */
    void addListener(const std::vector<std::string> & names,
        InputListener * listener);

    /*! Calls this function for all nested streams.
     */
    void activate();

    /*! Calls this function for all nested streams.
     */
    void deactivate();

    /*! Calls this function for all nested streams.
     */
    void stop();

    /*! Calls this function for all nested streams.
     */
    void wait();

  private:
    std::vector<boost::shared_ptr<dqm_core::Input> > input_;
  };
}

#endif
