#ifndef _DQM_CORE_PARAMETER_H_
#define _DQM_CORE_PARAMETER_H_

/*! \file Parameter.h Declares the dqm_core::Parameter class.
 * \author Serguei Kolos
 * \version 1.0
 */

#include <dqm_core/exceptions.h>
#include <dqm_core/Algorithm.h>
#include <dqm_core/AlgorithmConfig.h>
#include <dqm_core/Input.h>
#include <dqm_core/InputListener.h>
#include <dqm_core/ParameterConfig.h>
#include <dqm_core/Node.h>

/*! \namespace dqm_core
 *  This is a wrapping namespace for all public classes of the Data Quality Monitoring framework.
 */
namespace dqm_core {
  class Region;

  class Parameter: public InputListener, public Node {
    friend class Region;

  public:

    /*! This function can be used to enable/disable the current Parametr
     \sa void dqm_core::Node::stateChanged( bool enabled, bool propagate )
     */
    virtual void stateChanged(bool enabled, bool propagate);

    void reset(bool notify_parents, bool /* propagate */);

    /*! This function dumps the parameter to the given output stream
     */
    virtual void print(std::ostream & out) const;

  protected:
    Parameter(const std::string & name, Input & in, Output & out,
        boost::shared_ptr<Region> parent,
        const dqm_core::ParameterConfig & config);

    Parameter(const std::string & name, Output & out,
        boost::shared_ptr<Region> parent,
        const dqm_core::ParameterConfig & config);

    ~Parameter();

    /*! Always returns true.
     */
    bool isLeaf() const {
      return true;
    }

    void handleInput(const std::string & name, const TObject & object);

    void subscribe();

  protected:
    Input * input_;
    std::unique_ptr<Algorithm> algorithm_;
    std::shared_ptr<AlgorithmConfig> algorithm_config_;
    bool input_is_regex_;
    const std::vector<std::string> input_data_;
  };
}

#endif
