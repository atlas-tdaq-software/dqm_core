#ifndef _DQM_CORE_ALGORITHM_REGISTRATOR_H_
#define _DQM_CORE_ALGORITHM_REGISTRATOR_H_

/*! \file AlgorithmRegistrator.h Declares the dqm_core::OutputFactory interface.
 * \author Serguei Kolos
 * \version 1.0
 */

#include <dqm_core/AlgorithmManager.h>

/*! \namespace dqm_core
 *  This is a wrapping namespace for all public classes of the Data Quality Monitoring framework.
 */
namespace dqm_core {
  /*! \brief This class is a simple helper for registering user defined Algorithm and SummaryMaker objects.
   \ingroup public
   */
  template<class T>
  struct AlgorithmRegistrator {
    /*!
     In order to use this class a custom Algorithm (or SummaryMaker) must provide default constructor.
     A new instance of the custom Algorithm is created by this constructor and is registered with the
     dqm_core::AlgorithmManager singleton. This instance then will solely be used to call the clone()
     function to create an Algorithm which will be associated with dqm_core::Parameter or SummaryMaker
     attached to dqm_core::Region
     \param name name of the algorithm to be used for registration
     */
    AlgorithmRegistrator(const std::string & name) {
      dqm_core::AlgorithmManager::instance().registerAlgorithm(name, new T());
    }
  };
}

#endif
