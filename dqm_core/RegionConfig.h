#ifndef _DQM_CORE_REGION_CONFIG_H_
#define _DQM_CORE_REGION_CONFIG_H_

/*! \file RegionConfig.h Declares the dqm_core::RegionConfig class.
 * \author Serguei Kolos
 * \version 1.0
 */

#include <string>
#include <vector>

#include <dqm_core/AlgorithmConfig.h>
#include <dqm_core/NodeConfig.h>

/*! \namespace dqm_core
 *  This is a wrapping namespace for all public classes of the Data Quality Monitoring framework.
 */
namespace dqm_core {
  /*! \brief This class provides the configuration information for a DQM Region.
   \ingroup public
   \sa	dqm_core::ParameterConfig
   */
  struct RegionConfig: public NodeConfig {
  public:
    RegionConfig(const std::string & algorithm, float weight, bool enabled =
        true) :
        NodeConfig(algorithm, weight, enabled) {
      ;
    }
  };
}

#endif
