#ifndef _DQM_CORE_PARAMETER_CONFIG_H_
#define _DQM_CORE_PARAMETER_CONFIG_H_

/*! \file ParameterConfig.h Declares the dqm_core::ParameterConfig class.
 * \author Serguei Kolos
 * \version 1.0
 */

#include <string>
#include <vector>

#include <boost/shared_ptr.hpp>

#include <dqm_core/AlgorithmConfig.h>
#include <dqm_core/NodeConfig.h>

/*! \namespace dqm_core
 *  This is a wrapping namespace for all public classes of the Data Quality Monitoring framework.
 */
namespace dqm_core {
  /*! \brief This class provides the configuration information for a DQM Parameter.
   \ingroup public
   \sa	dqm_core::RegionConfig
   */
  struct ParameterConfig: public NodeConfig {
    ParameterConfig(const std::string & inputData,
        const std::string & algorithm, float weight,
        const std::shared_ptr<AlgorithmConfig> & aconfig, bool input_is_regex =
            false, bool enabled = true) :
        NodeConfig(algorithm, weight, enabled), input_data_(1, inputData), algorithm_config_(
            aconfig), input_is_regex_(input_is_regex) {
      ;
    }

    ParameterConfig(const std::vector<std::string> & inputData,
        const std::string & algorithm, float weight,
        const std::shared_ptr<AlgorithmConfig> & aconfig, bool enabled = true) :
        NodeConfig(algorithm, weight, enabled), input_data_(inputData), algorithm_config_(
            aconfig), input_is_regex_(false) {
      ;
    }

    /*! This function dumps the object to the given output stream
     */
    void print(std::ostream & out) const;

    const std::vector<std::string> input_data_;
    std::shared_ptr<AlgorithmConfig> algorithm_config_;
    bool input_is_regex_;
  };
}

#endif
