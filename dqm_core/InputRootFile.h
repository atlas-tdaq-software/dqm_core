#ifndef _DQM_CORE_INPUT_ROOT_FILE_H_
#define _DQM_CORE_INPUT_ROOT_FILE_H_

/*! \file Input.h Declares the dqm_core::InputRootFile class.
 * \author Michael Wilson
 * \version 1.0
 */

#include <map>
#include <memory>
#include <string>
#include <vector>

#include <TFile.h>

#include <dqm_core/Input.h>
#include <dqm_core/exceptions.h>

class TDirectory;
class TKey;

namespace dqm_core {
  class InputRootFile: public Input {
  public:

    InputRootFile(const std::string & rootFileName);
    InputRootFile(const std::vector<std::string> & rootFileName);
    ~InputRootFile();

    virtual void addListener(const std::string & name,
        InputListener * listener);

    virtual void addListener(const boost::regex & regex,
        InputListener * listener);

    virtual void addListener(const std::vector<std::string> & names,
        InputListener * listener);

    virtual void activate();
    virtual void deactivate();
    virtual void stop();
    virtual void wait();

  protected:

    struct MonBundle {
      std::string pathAndName;
      TKey* key;
      InputListener* listener;

      MonBundle(const std::string& pathAndName_, TKey* key_,
          InputListener* listener_) :
          pathAndName(pathAndName_), key(key_), listener(listener_) {
      }
    };

    typedef std::vector<MonBundle> MonContainer;
    typedef MonContainer::const_iterator MonContainerIter;

    typedef std::map<std::string, TDirectory*> DirMap_t;

    static TKey* getObjKey(TDirectory* dir, std::string path);
    static TKey* getObjKey(DirMap_t& dirmap, TDirectory* dir,
        std::string leadingPath, std::string path);

    MonContainer m_monObjects;
    DirMap_t m_allDirs;

    std::string m_fileName;
    std::unique_ptr<TFile> m_file;

  private:
    InputRootFile();
  };
} // namespace dqm_core

#endif
