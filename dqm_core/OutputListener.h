#ifndef _DQM_CORE_OUTPUT_LISTENER_H_
#define _DQM_CORE_OUTPUT_LISTENER_H_

/*! \file OutputListener.h Declares the dqm_core::OutputListener class.
 * \author Serguei Kolos
 * \version 1.0
 */

#include <string>

#include <dqm_core/Output.h>

/*! \namespace dqm_core
 *  This is a wrapping namespace for all public classes of the Data Quality Monitoring framework.
 */
namespace dqm_core {
  /*! \brief This class defines abstract interface for the notification of availability
   of the Data Quality assessment result. User has to declare a
   class which implements this interface and register instances of such class
   with the dqm_core::Output class via one of the dqm_core::Output::addListener functions.
   \ingroup public
   \sa	dqm_core::Output
   */
  struct OutputListener {
    /*! Virtual destructor is defined to make sure that destructor of the sub-class will be
     called whenever the delete operator is applied to the dqm_core::OutputListener pointer.
     */
    virtual ~OutputListener() {
      ;
    }

    /*! This function is called whenever an object for which this listener has been
     registered appears in the corresponding dqm_core::Output stream.
     \param name objects's name
     \param object DQ assessment result
     */
    virtual void handleResult(const std::string & name,
        const Result & object) = 0;

  };
}

#endif
