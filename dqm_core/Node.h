#ifndef _DQM_CORE_NODE_H_
#define _DQM_CORE_NODE_H_

/*! \file Node.h Declares the dqm_core::Node class.
 * \author Serguei Kolos
 * \version 1.0
 */

#include <string>

#include <boost/enable_shared_from_this.hpp>
#include <boost/shared_ptr.hpp>
#include <boost/thread/mutex.hpp>

#include <dqm_core/exceptions.h>
#include <dqm_core/NodeConfig.h>
#include <dqm_core/Output.h>
#include <dqm_core/Result.h>
#include <dqm_core/StateChangeListener.h>

/*! \namespace dqm_core
 *  This is a wrapping namespace for all public classes of the Data Quality Monitoring framework.
 */
namespace dqm_core {
  class Region;

  class Node: public StateChangeListener, public boost::enable_shared_from_this<
      Node>, boost::noncopyable {
    friend class Region;

  public:
    virtual ~Node();

    /*! Returns name of this Node.
     */
    const std::string & getName() const {
      return name_;
    }

    /*! Returns algorithm name for this Node.
     */
    const std::string & getAlgorithmName() const {
      return algorithm_name_;
    }

    /*! Returns last DQ result calculated for this Node.
     */
    boost::shared_ptr<Result> getResult() const;

    /*! Returns weight of this Node.
     */
    float getWeight() const {
      return weight_;
    }

    /*! Returns depth of this node in the DQ Tree.
     */
    int depth() const;

    /*! Returns number of Regions referencing this Node.
     */
    size_t parentsNumber() const {
      return parents_.size();
    }

    /*! This function returns <b>this</b> pointer if the current parameter has the given name.
     Otherwise it returns zero.
     \param name parameter name
     \return 	zero if there is no region/parameter associated with the given name,
     otherwise a pointer to the target object
     */
    virtual boost::shared_ptr<Node> find(const std::string & name);

    /*! This function resets the result of this Node to the Undefined status.
     If the Result of this Node has Disabled status then it will not be changed.
     \param notify_parents 	if the value is <b>true</b> then will call the handleResult on the parent Regions
     \param propagate 		has no effect
     */
    virtual void reset(bool notify_parents, bool propagate);

    /*! This function can be used to enable/disable the current Node. A Node is always
     constructed with enabled state. If the Node is disabled then its status is set to
     dqm_core::Result::Disabled and the Algorithm associated with it will not be called
     util it is re-enabled again. If the Node is enabled its state will be set to
     dqm_core::Result::Unknown and the Algorithm will be called as soon as the input
     histogram is updated.
     \param enabled if the value is <b>true</b> then the parameter will be enabled, otherwise
     it will be disabled
     \param propagate 	has no effect for Node objects
     \sa void dqm_core::Region::stateChanged( bool enabled, bool propagate )
     */
    virtual void stateChanged(bool enabled, bool propagate);

    /*! This function dumps this parameter to the given output stream
     */
    virtual void print(std::ostream & out) const;

    /*! This function returns true for parameters and false for regions
     */
    virtual bool isLeaf() const = 0;

  protected:
    Node(const std::string & name, Output & out,
        boost::shared_ptr<Region> parent, const dqm_core::NodeConfig & config);

    void addParent(const boost::shared_ptr<Region> & parent);

    void notifyParents(const Result & result);

  protected:
    typedef std::vector<boost::weak_ptr<Region> > ParentsList;

  protected:
    mutable boost::mutex mutex_;
    const std::string name_;
    const std::string algorithm_name_;
    const float weight_;
    bool enabled_;
    Output & output_;
    ParentsList parents_;
    boost::shared_ptr<Result> result_;
  };
}

#endif
