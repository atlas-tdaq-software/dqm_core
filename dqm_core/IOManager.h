/*
 *  IOManager.h
 *  dqm_core
 *
 *  Created by Serguei Kolos on 21.11.08.
 *  Copyright 2005 CERN. All rights reserved.
 *
 */

#ifndef DQM_CORE_IO_MANAGER_H
#define DQM_CORE_IO_MANAGER_H

#include <map>
#include <boost/shared_ptr.hpp>
#include <boost/thread.hpp>
#include <boost/noncopyable.hpp>

#include <ers/internal/SingletonCreator.h>
#include <dqm_core/exceptions.h>

/** \file IOManager.h This file defines the IOManager class, 
 * which is responsible for registration of DQM streams.
 * \author Serguei Kolos
 * \brief dqm_core header and documentation file
 */

namespace dqm_core {
  class Output;
  class Input;
  class OutputFactory;
  class InputFactory;

  /** The \c IOManager class is responsible for creating and handling all the DQM
   * streams used by an application. It implements the singleton pattern and handles a
   * table of the available streams.
   *
   * \author Serguei Kolos
   * \version 1.0
   * \brief Manager of DQM streams.
   */

  class IOManager: boost::noncopyable {
    template<class > friend class ers::SingletonCreator;
  public:

    ~IOManager();

    static IOManager & instance(); /**< \brief return the singleton */

    dqm_core::Input * createInput(const std::string & name,
        const std::vector<std::string> & params);
    dqm_core::Output * createOutput(const std::string & name,
        const std::vector<std::string> & param);

    void registerInputFactory(const std::string & name,
        dqm_core::InputFactory * factory);
    void registerOutputFactory(const std::string & name,
        dqm_core::OutputFactory * factory);

  private:
    IOManager();

    typedef std::map<std::string, boost::shared_ptr<dqm_core::InputFactory> > InputFactories;
    typedef std::map<std::string, boost::shared_ptr<dqm_core::OutputFactory> > OutputFactories;

    mutable boost::mutex m_mutex;
    InputFactories m_input_factories;
    OutputFactories m_output_factories;
  };
}

#endif

